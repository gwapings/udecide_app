package org.app.udecide;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.app.udecide.Adapter.AOMenuAdapter;
import org.app.udecide.CustomView.CustomImageView;
import org.app.udecide.Model.Restaurant;
import org.app.udecide.Model.RestaurantMenu;
import org.app.udecide.Model.Thumbnail;
import org.app.udecide.Parser.RestaurantParser;
import org.app.udecide.helper.AsyncBitmapLoader;
import org.app.udecide.helper.UDecideConstants;
import org.app.udecide.helper.UDecideHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import vibetech.helper.httpclienthelper.HEADERS;
import vibetech.helper.httpclienthelper.HttpClientHelper;
import vibetech.helper.httpclienthelper.HttpClientInterface;
import vibetech.helper.httpclienthelper.ToastHandler;

public class FragmentAdvanceOrder extends Fragment implements AOMenuAdapter.OnAOMenuListener {
    private OnFragmentInteractionListener mListener;

    private Restaurant mRestaurant;

    protected View mView;
    protected ListView mListMenu;
    protected AOMenuAdapter mMenuAdapter;
    protected Boolean is_delivery = false;
    protected LinearLayout mDeliveryDetailsContainer;
    protected EditText mDeliveryAddress;
    protected EditText mDeliveryContactNumber;
    protected RadioGroup mRdoGrpAO;

    private ArrayList<RestaurantMenu> mMenus = new ArrayList<RestaurantMenu>();

    public FragmentAdvanceOrder() {
    }

    public static FragmentAdvanceOrder newInstance(String param1, String param2) {
        FragmentAdvanceOrder fragment = new FragmentAdvanceOrder();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mRestaurant = (Restaurant) getArguments().getParcelable("restaurant");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_advance_order, container, false);
            render();
        }
        return mView;
    }

    protected void render() {
        mDeliveryDetailsContainer = (LinearLayout) mView.findViewById(R.id.rel_delivery_details);
        mDeliveryDetailsContainer.setVisibility(View.GONE);

        mDeliveryAddress = (EditText) mView.findViewById(R.id.delivery_address);
        mDeliveryContactNumber = (EditText) mView.findViewById(R.id.delivery_contact_number);

        TextView txtRestoName = (TextView) mView.findViewById(R.id.ao_resto_name);
        txtRestoName.setText(mRestaurant.getResto_name() + " Menu List");

        Button mBtnSubmit = (Button) mView.findViewById(R.id.ao_btn_submit);
        mBtnSubmit.setOnClickListener(onSubmitListener);

        mListMenu = (ListView) mView.findViewById(R.id.ao_list_menu);
        mMenuAdapter = new AOMenuAdapter(getContext(), this);
        mListMenu.setAdapter(mMenuAdapter);
        mListMenu.setOnItemClickListener(menu_item_listener);

        mRdoGrpAO = (RadioGroup) mView.findViewById(R.id.rdo_grp_ao);
        mRdoGrpAO.setOnCheckedChangeListener(onRdoClickListener);

        RadioButton rdoDelivery = (RadioButton) mView.findViewById(R.id.rdo_delivery);

        if (mRestaurant.isHas_delivery()) {
            rdoDelivery.setVisibility(View.VISIBLE);
        } else {
            rdoDelivery.setVisibility(View.GONE);
        }

        if (mRestaurant.getStore_status().equalsIgnoreCase("closed")) {
            mBtnSubmit.setVisibility(View.GONE);
        } else {
            mBtnSubmit.setVisibility(View.VISIBLE);
        }

        getMenuList();
    }

    private RadioGroup.OnCheckedChangeListener onRdoClickListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
            is_delivery = false;
            mDeliveryDetailsContainer.setVisibility(View.GONE);

            RadioButton rdoBtn = (RadioButton) mView.findViewById(checkedId);
            if (rdoBtn.getText().toString().equalsIgnoreCase("for delivery")) { // For delivery
                is_delivery = true;
                mDeliveryDetailsContainer.setVisibility(View.VISIBLE);
            } else if (rdoBtn.getText().toString().equalsIgnoreCase("for pickup")) { // For Pickup
                is_delivery = false;
            } else {
                // back to client dashboard activity
                getActivity().getSupportFragmentManager().beginTransaction().remove(FragmentAdvanceOrder.this).commit();
                getActivity().getSupportFragmentManager().popBackStack();

                Bundle bundle = new Bundle();
                bundle.putParcelable("restaurant", mRestaurant);
                bundle.putBoolean("is_request_seat", true);

                Fragment mFragment = new FragmentRestaurantMenu();
                mFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frag_container, mFragment, "restaurant_menu")
                        .addToBackStack("restaurant_menu")
                        .commit();
            }
        }
    };

    protected AdapterView.OnItemClickListener menu_item_listener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            RestaurantMenu menu = (RestaurantMenu) parent.getItemAtPosition(position);
            ToastHandler.ShowToast(getContext(), menu.getFood_name());
        }
    };

    protected View.OnClickListener onSubmitListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.d("order", "[onSubmitListener][orders_count]|-> " + countAOrders(mMenus));
            if (countAOrders(mMenus) > 0) {
                setupAOParams();
            } else {
                ToastHandler.ShowToast(getContext(), "No Selected Order!");
            }
        }
    };

    protected void setupAOParams() {
        try {
            final JSONObject JSONParams = new JSONObject();
            JSONArray arrOrders = new JSONArray();

            for (RestaurantMenu menu : mMenus) {
                if (menu.is_Checked()) {
                    Log.d("order", "\n\n\n-------------------------------------------------------");
                    Log.d("order", "[onMenuItemCheckSelect][menu][name]|===>" + menu.getFood_name());
                    Log.d("order", "[onMenuItemCheckSelect][menu][order_count]|===>" + menu.getOrder_item_count());
                    Log.d("order", "[onMenuItemCheckSelect][menu][is_checked]|===>" + menu.is_Checked());

                    JSONObject objOrder = new JSONObject();
                    objOrder.put("menu_id", menu.getId());
                    objOrder.put("item_count", menu.getOrder_item_count());
                    arrOrders.put(objOrder);
                }
            }
            JSONParams.put("user_id", Integer.parseInt(new UDecideHelper().getUserId(getContext())));
            JSONParams.put("restaurant_id", mRestaurant.getId());
            JSONParams.put("orders", arrOrders);
            JSONParams.put("is_delivery", (is_delivery) ? 1 : 0);
            if (is_delivery) {
                String deliveryAddress = mDeliveryAddress.getText().toString();
                String deliveryContactNumber = mDeliveryContactNumber.getText().toString();
                if (deliveryAddress.isEmpty() || deliveryContactNumber.isEmpty()) {
                    ToastHandler.ShowToast(getContext(), "Delivery details must not be empty!");
                    return;
                } else {
                    JSONObject objDelivery = new JSONObject();
                    objDelivery.put("address", deliveryAddress);
                    objDelivery.put("contact_number", deliveryContactNumber);

                    JSONParams.put("delivery_details", objDelivery);
                }
            }
            Log.d("ao", "[onMenuItemCheckSelect][menu][JSONParams]|===>" + JSONParams.toString());

            AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
            mBuilder.setTitle("Advance Order");
            mBuilder.setMessage("Are you sure do you want to submit this transaction?");
            mBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    postAdvanceOrder(JSONParams);
                }
            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            mBuilder.create().show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void postAdvanceOrder(JSONObject params) {
        Log.d("order", "[onSubmitListener][json_request]|-> " + params.toString());

        String order_url = UDecideConstants.BASE_URL + "api/restaurant/advance/order";
        new HttpClientHelper().executeInBackground(getContext(), order_url, params, null, true, true, new ArrayList<HEADERS>(), new HttpClientInterface() {
            ProgressDialog loader;

            @Override
            public void onBeforeExecute(int i) {
                loader = ProgressDialog.show(getActivity(), "", "loading....");
            }

            @Override
            public void onHttpClientResponse(int i, String s) {
                Log.d("order", "[postAdvanceOrder][onHttpClientResponse][response]|===>" + s);
                loader.setMessage("please wait....");
            }

            @Override
            public void onHttpClientErrorException(int i, Exception e) {
                Log.d("order", "[postAdvanceOrder][onHttpClientErrorException][error]|===>" + e.toString());
                loader.setMessage(e.getMessage());
                loader.dismiss();
            }

            @Override
            public void onAfterExecute(int i) {
                loader.dismiss();
                ToastHandler.ShowToast(getContext(), "Transaction Completed");

                // back to client dashboard activity
                getActivity().getSupportFragmentManager().beginTransaction().remove(FragmentAdvanceOrder.this).commit();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        }, 10001);
    }

    protected void getMenuList() {
        if (mRestaurant == null) {
            getMenuList();
        }
        HEADERS headers = new HEADERS();
        headers.addHeaders("UDecide_USERID", new UDecideHelper().getUserId(getContext()));
        String menu_url = UDecideConstants.BASE_URL + "api/restaurant/" + mRestaurant.getId() + "/menus";
        new HttpClientHelper().executeInBackground(getContext(), menu_url, null, null, false, false, headers.getArrHeaders(), new HttpClientInterface() {
            @Override
            public void onBeforeExecute(int i) {
            }

            @Override
            public void onHttpClientResponse(int i, String s) {
                mMenuAdapter.updateMenus(new RestaurantParser().parseRestaurantMenu(s));
            }

            @Override
            public void onHttpClientErrorException(int i, Exception e) {
            }

            @Override
            public void onAfterExecute(int i) {
            }
        }, 1000);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onMenuImageClick(Thumbnail thumbnail, View view) {
        String thumbnail_path = null;
        thumbnail_path = thumbnail.getFile_path();
        if (thumbnail_path != null) {
            showImagePreview(thumbnail_path);
        }
    }

    @Override
    public void onMenuItemCheckSelect(ArrayList<RestaurantMenu> menus) {
        mMenus = menus;
    }

    @Override
    public void onMenuQuantityChanged(ArrayList<RestaurantMenu> menus) {
        mMenus = menus;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private int countAOrders(ArrayList<RestaurantMenu> menus) {
        int order = 0;
        for (RestaurantMenu menu : menus) {
            if (menu.is_Checked()) {
                order++;
            }
        }
        return order;
    }

    protected void showImagePreview(String image_path) {
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        CustomImageView imageView = new CustomImageView(getContext());

        if (AsyncBitmapLoader.cancelPotentialWork(UDecideConstants.BASE_URL + image_path, imageView)) {
            AsyncBitmapLoader task = new AsyncBitmapLoader(getContext(), imageView, 0, 0);
            AsyncBitmapLoader.AsyncDrawable asyncDrawable =
                    new AsyncBitmapLoader.AsyncDrawable(getResources(), UDecideConstants.getDefaultImagePlaceholder(getContext()), task);
            imageView.setImageDrawable(asyncDrawable);
            task.execute(UDecideConstants.BASE_URL + image_path);
        }

        RelativeLayout.LayoutParams lParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        builder.addContentView(imageView, lParams);
        builder.show();
    }
}

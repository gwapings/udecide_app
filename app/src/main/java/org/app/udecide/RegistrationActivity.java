package org.app.udecide;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import org.app.udecide.helper.UDecideConstants;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import vibetech.helper.httpclienthelper.HEADERS;
import vibetech.helper.httpclienthelper.HttpClientHelper;
import vibetech.helper.httpclienthelper.HttpClientInterface;
import vibetech.helper.httpclienthelper.ToastHandler;

public class RegistrationActivity extends AppCompatActivity implements HttpClientInterface {

    protected ProgressDialog loader;

    protected AutoCompleteTextView vwUsername;
    protected EditText vwPassword;
    protected EditText vwEmailAddress;
    protected EditText vwFullname;
    protected EditText vwAddress;
    protected EditText vwContactNo;
    protected Spinner vwGender;
    protected Button vwBtnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registration);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        fab.setVisibility(View.GONE);

        this.initializeViewsComponent();
    }

    private void initializeViewsComponent() {
        this.vwUsername = (AutoCompleteTextView) findViewById(R.id.foodie_username);
        this.vwPassword = (EditText) findViewById(R.id.foodie_password);
        this.vwFullname = (EditText) findViewById(R.id.foodie_full_name);
        this.vwAddress = (EditText) findViewById(R.id.foodie_address);
        this.vwContactNo = (EditText) findViewById(R.id.foodie_contact_no);
        this.vwEmailAddress = (EditText) findViewById(R.id.foodie_email);
        this.vwGender = (Spinner) findViewById(R.id.foodie_gender);

        this.vwBtnSubmit = (Button) findViewById(R.id.submit_registration);
        this.vwBtnSubmit.setOnClickListener(onBtnSubmitClicked);
    }

    private View.OnClickListener onBtnSubmitClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            submitRegistration();
        }
    };

    private void clearField() {
        vwUsername.getText().clear();
        vwPassword.getText().clear();
        vwFullname.getText().clear();
        vwAddress.getText().clear();
        vwContactNo.getText().clear();
        vwEmailAddress.getText().clear();
        vwGender.setSelection(0);

        vwUsername.requestFocus();
    }

    private void submitRegistration() {
        try {
            if (vwUsername.getText().toString().isEmpty() ||
                    vwPassword.getText().toString().isEmpty() ||
                    vwFullname.getText().toString().isEmpty() ||
                    vwAddress.getText().toString().isEmpty() ||
                    vwContactNo.getText().toString().isEmpty() ||
                    vwEmailAddress.getText().toString().isEmpty() ||
                    vwGender.getSelectedItem().toString().isEmpty()) {
                ToastHandler.ShowToast(this, "Please fill in all fields.");
            } else {
                JSONObject objReg = new JSONObject();
                objReg.put("username", vwUsername.getText().toString());
                objReg.put("password", vwPassword.getText().toString());
                objReg.put("name", vwFullname.getText().toString());
                objReg.put("address", vwAddress.getText().toString());
                objReg.put("contact_no", vwContactNo.getText().toString());
                objReg.put("email", vwEmailAddress.getText().toString());
                objReg.put("gender", vwGender.getSelectedItem().toString());

                Log.d("TEST", "JSON MSG: " + objReg.toString());

                String submitUrl = UDecideConstants.BASE_URL + "api/register/foodie";
                new HttpClientHelper().executeInBackground(this, submitUrl, objReg, null, true, true, new ArrayList<HEADERS>(), this, 10001);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("TEST", "ERROR[submitRegistration] -> " + e.getMessage());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.finish();
    }

    @Override
    public void onBeforeExecute(int i) {
        Log.d("TEST", "(onBeforeExecute) i - value -> " + i);
        loader = new ProgressDialog(RegistrationActivity.this);
        loader.show();
        loader.setMessage("please wait....");
    }

    @Override
    public void onHttpClientResponse(int i, String s) {
        loader.setMessage("loading, please wait....");
        Log.d("TEST", "(onHttpClientResponse) i - value -> " + i);
        Log.d("TEST", "(onHttpClientResponse) String s -> " + s);
        if (i == 1000) {
            try {
                parseResponse(s);
            } catch (JSONException e) {
                e.printStackTrace();
                loader.setMessage(e.toString());
            } catch (InterruptedException e) {
                e.printStackTrace();
                loader.setMessage(e.toString());
            }
        } else if (i == 10001) {
            ToastHandler.ShowToast(RegistrationActivity.this, "Registration Completed.");
        }
    }

    @Override
    public void onHttpClientErrorException(int i, Exception e) {
        Log.d("TEST", "(onHttpClientErrorException) i - value -> " + i);
        Log.d("TEST", "(onHttpClientErrorException) msg - value -> " + e.toString());
        loader.setMessage(e.getMessage());
        loader.dismiss();
        ToastHandler.ShowToast(RegistrationActivity.this, e.toString());
    }

    @Override
    public void onAfterExecute(int i) {
        Log.d("TEST", "(onAfterExecute) i - value -> " + i);
        loader.dismiss();

        if (i == 10001) {
            String url = UDecideConstants.BASE_URL + "api/verify/login";
            HEADERS headers = new HEADERS();
            headers.addHeaders("UDecide_USERNAME", vwUsername.getText().toString());
            headers.addHeaders("UDecide_PASSWORD", vwPassword.getText().toString());
            for (HEADERS h : headers.getArrHeaders()) {
                Log.d("TEST", "[Header] |-> " + h.getHeaderValue());
            }
            new HttpClientHelper().executeInBackground(this, url, null, null, false, false, headers.getArrHeaders(), this, 1000);
        }
    }

    protected void parseResponse(String json) throws JSONException, InterruptedException {
        Thread.sleep(3000);
        JSONObject response = new JSONObject(json);
        String status = response.getString("status");
        String user_type = response.getJSONObject("data").getString("user_type");

        if (status.equalsIgnoreCase("success") && user_type.equalsIgnoreCase("foodie")) {
            SharedPreferences prefs = this.getSharedPreferences(
                    "login_preferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("is_logged_in", true);
            editor.putBoolean("is_guest", false);
            editor.putString("response", response.toString());
            editor.commit();

            startActivity(new Intent(this, ClientDashboardActivity.class));
            this.finish();
        } else {
            ToastHandler.ShowToast(this, response.getString("error_message"));
            clearField();
        }
    }
}

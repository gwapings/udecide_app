package org.app.udecide;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import org.app.udecide.Adapter.RestaurantListAdapter;
import org.app.udecide.Model.Categories;
import org.app.udecide.Model.Restaurant;
import org.app.udecide.Parser.RestaurantParser;
import org.app.udecide.helper.UDecideBackgroundService;
import org.app.udecide.helper.UDecideConstants;
import org.app.udecide.helper.UDecideHelper;
import org.app.udecide.helper.UDecideLocationListener;

import java.util.ArrayList;
import java.util.Comparator;

import vibetech.helper.httpclienthelper.HEADERS;
import vibetech.helper.httpclienthelper.HttpClientHelper;
import vibetech.helper.httpclienthelper.HttpClientInterface;
import vibetech.helper.httpclienthelper.ToastHandler;

public class FragmentRestaurants extends Fragment {
    private OnFragmentInteractionListener mListener;

    private Context mContext;
    private View mView;
    private ListView mLRestaurant;
    private RestaurantListAdapter mLRestaurantAdapter;

    private MenuItem mSearchAction;

    protected Categories mCategory;
    protected Restaurant mRestaurant;
    protected HttpClientHelper mHttpHelper;
    protected Location mLastLocation;
    protected UDecideHelper mHelper;

    protected Boolean isNearBy = false;

    public FragmentRestaurants() {
    }

    public static FragmentRestaurants newInstance(String param1, String param2) {
        FragmentRestaurants fragment = new FragmentRestaurants();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        getContext().registerReceiver(receiver, new IntentFilter(
                UDecideBackgroundService.UDecideBroadcaster));
    }

    @Override
    public void onPause() {
        super.onPause();
        getContext().unregisterReceiver(receiver);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mHttpHelper = new HttpClientHelper();
        mHelper = new UDecideHelper();

        if (getArguments() != null) {
            isNearBy = getArguments().getBoolean("is_nearby");
            if (isNearBy) {
                mLastLocation = (Location) getArguments().getParcelable("mLastLocation");
            }
            mCategory = (Categories) getArguments().getParcelable("category");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.mView = inflater.inflate(R.layout.restaurants, container, false);
        initializeView();
        getRestaurants();
        return this.mView;
    }

    private void initializeView() {
        this.mLRestaurant = (ListView) this.mView.findViewById(R.id.restaurant_lView);
        this.mLRestaurant.setOnItemClickListener(restoItemClickListener);
        this.mLRestaurantAdapter = new RestaurantListAdapter(getActivity().getApplicationContext(), new ArrayList<Restaurant>());
        this.mLRestaurant.setAdapter(this.mLRestaurantAdapter);
    }

    private AdapterView.OnItemClickListener restoItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            if (!new UDecideHelper().isGuest(getActivity())) {
                mRestaurant = (Restaurant) adapterView.getItemAtPosition(i);

                Bundle bundle = new Bundle();
                bundle.putParcelable("restaurant", mRestaurant);
                bundle.putBoolean("is_request_seat", false);

                Fragment mFragment = new FragmentRestaurantMenu();
                mFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frag_container, mFragment, "restaurant_menu")
                        .addToBackStack("restaurant_menu")
                        .commit();
            } else {
                showAlertRegistration();
            }
        }
    };

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void showAlertRegistration() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
        mBuilder.setTitle("Sign-Up Now?");
        mBuilder.setMessage("You need to signup before you can access the full content.");
        mBuilder.setPositiveButton("Sign Up", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new UDecideHelper().logout(getActivity());

                startActivity(new Intent(getActivity(), RegistrationActivity.class));
                getActivity().finish();
            }
        }).setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        mBuilder.create().show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mSearchAction = menu.findItem(R.id.action_search);
        mSearchAction.setVisible(true);

        SearchView searchView = (SearchView) mSearchAction.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                HEADERS headers = new HEADERS();
                headers.addHeaders("UDecide_USERID", new UDecideHelper().getUserId(getContext()));
                String restaurantSearchURL = UDecideConstants.BASE_URL + "api/restaurant/list/search/" + query;
                mHttpHelper.executeInBackground(getActivity(), restaurantSearchURL, null, null, false, false, headers.getArrHeaders(), new HttpClientInterface() {
                    ProgressDialog loader;

                    @Override
                    public void onBeforeExecute(int i) {
                        Log.d("TEST", "(onBeforeExecute) i - value -> " + i);
                        loader = ProgressDialog.show(getActivity(), "", "loading....");
                    }

                    @Override
                    public void onHttpClientResponse(int i, String s) {
                        loader.setMessage("please wait....");
                        Log.d("TEST", "(onHttpClientResponse) i - value -> " + i);
                        Log.d("TEST", "(onHttpClientResponse) String s -> " + s);

                        ArrayList<Restaurant> arrRestaurants = new RestaurantParser().parseRestaurant(s);
                        updateRestaurantData(arrRestaurants);
                    }

                    @Override
                    public void onHttpClientErrorException(int i, Exception e) {
                        Log.d("TEST", "(onHttpClientErrorException) i - value -> " + i);
                        Log.d("TEST", "(onHttpClientErrorException) i - msg -> " + e.toString());
                        ToastHandler.ShowToast(getActivity(), e.toString());
                        loader.setMessage(e.getMessage());
                        loader.dismiss();

                        // back to client dashboard activity
                        getActivity().getSupportFragmentManager().beginTransaction().remove(FragmentRestaurants.this).commit();
                        getActivity().getSupportFragmentManager().popBackStack();
                    }

                    @Override
                    public void onAfterExecute(int i) {
                        Log.d("TEST", "(onAfterExecute) i - value -> " + i);
                        loader.dismiss();
                    }
                }, 1002);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.length() == 0) {
                    getRestaurants();
                }

                return false;
            }

        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void getRestaurants() {
        HEADERS headers = new HEADERS();
        headers.addHeaders("UDecide_USERID", new UDecideHelper().getUserId(getContext()));
        String restaurantListURL = UDecideConstants.BASE_URL + "api/restaurant/list";
        new HttpClientHelper().executeInBackground(getActivity(), restaurantListURL, null, null, false, false, headers.getArrHeaders(), new HttpClientInterface() {
            ProgressDialog loader;

            @Override
            public void onBeforeExecute(int i) {
//                Log.d("TEST", "(onBeforeExecute) i - value -> " + i);
                loader = ProgressDialog.show(getActivity(), "", "loading....");
            }

            @Override
            public void onHttpClientResponse(int i, String s) {
                loader.setMessage("please wait....");
//                Log.d("TEST", "(onHttpClientResponse) i - value -> " + i);
//                Log.d("TEST", "(onHttpClientResponse) String s -> " + s);

                ArrayList<Restaurant> arrRestaurants = new RestaurantParser().parseRestaurant(s);
                updateRestaurantData(arrRestaurants);
            }

            @Override
            public void onHttpClientErrorException(int i, Exception e) {
//                Log.d("TEST", "(onHttpClientErrorException) i - value -> " + i);
//                Log.d("TEST", "(onHttpClientErrorException) i - msg -> " + e.toString());
                ToastHandler.ShowToast(getActivity(), e.toString());
                loader.setMessage(e.getMessage());
                loader.dismiss();

                // back to client dashboard activity
                getActivity().getSupportFragmentManager().beginTransaction().remove(FragmentRestaurants.this).commit();
                getActivity().getSupportFragmentManager().popBackStack();
            }

            @Override
            public void onAfterExecute(int i) {
//                Log.d("TEST", "(onAfterExecute) i - value -> " + i);
                loader.dismiss();
            }
        }, 1001);
    }

    private void updateRestaurantData(ArrayList<Restaurant> restaurants) {
        Log.d("TEST", "[BroadcastReceiver][location][isNearBy] |-> " + isNearBy);
        if (!isNearBy) {
            mLRestaurantAdapter.updateRestaurantList(restaurants);
        } else {
            if (mCategory == null) { // is nearby filter
                ArrayList<Restaurant> nearbyRestaurants = new ArrayList<Restaurant>();
                for (Restaurant resto : restaurants) {
                    if (mLastLocation != null) {
                        double distance = mHelper.getDistanceBetweenLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), resto.getLocation().getLatitude(), resto.getLocation().getLongitude());
                        Log.d("TEST", "restaurant |-> " + resto.getResto_name());
                        Log.d("TEST", "distance |-> " + distance);
                        if (distance <= UDecideConstants.NEARBY_DISTANCE) {
                            nearbyRestaurants.add(resto);
                        }
                    }
                }
                mLRestaurantAdapter.updateRestaurantList(nearbyRestaurants);
            } else {
                ArrayList<Restaurant> filteredRestaurants = new ArrayList<Restaurant>();
                for (Restaurant resto : restaurants) {
                    String category_name = mCategory.getName();
                    if (resto.getCategory().getType().contains(category_name)) {
                        filteredRestaurants.add(resto);
                    }
                }
                mLRestaurantAdapter.updateRestaurantList(filteredRestaurants);
            }
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Location location = (Location) intent.getExtras().getParcelable("location_listener");
            if (location != null) {
                mLastLocation = location;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLRestaurantAdapter.updateRestaurantDistance(location);
                    }
                });
            }
        }
    };

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

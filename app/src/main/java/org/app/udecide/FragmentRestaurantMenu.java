package org.app.udecide;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.app.udecide.CustomView.CustomImageDivider;
import org.app.udecide.CustomView.CustomImageView;
import org.app.udecide.Model.Restaurant;
import org.app.udecide.Model.RestaurantMenu;
import org.app.udecide.Model.Thumbnail;
import org.app.udecide.Parser.RestaurantParser;
import org.app.udecide.helper.AsyncBitmapLoader;
import org.app.udecide.helper.MapRouteDirections;
import org.app.udecide.helper.UDecideBackgroundService;
import org.app.udecide.helper.UDecideConstants;
import org.app.udecide.helper.UDecideHelper;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import vibetech.helper.httpclienthelper.HEADERS;
import vibetech.helper.httpclienthelper.HttpClientHelper;
import vibetech.helper.httpclienthelper.HttpClientInterface;
import vibetech.helper.httpclienthelper.ToastHandler;

public class FragmentRestaurantMenu extends Fragment implements OnMapReadyCallback, HttpClientInterface {

    private OnFragmentInteractionListener mListener;

    protected View mView;
    protected LinearLayout mLinearParent;
    protected Button mBtnDirections;

    protected GoogleMap mGoogleMap;
    protected Restaurant mRestaurant;

    protected TextView mTxtRestaurantDistance;
    protected Button mBtnCheckin;
    protected Button mBtnReserve;
    protected int remainingSeat;

    protected MapRouteDirections mRouteDirections;
    protected Location mCurrentLocation;

    public FragmentRestaurantMenu() {
    }

    public static FragmentRestaurantMenu newInstance() {
        FragmentRestaurantMenu fragment = new FragmentRestaurantMenu();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mRestaurant = (Restaurant) getArguments().getParcelable("restaurant");

            boolean isRequestSeat = getArguments().getBoolean("is_request_seat");
            if (isRequestSeat) {
                seatReservation();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (this.mView == null) {
            this.mView = inflater.inflate(R.layout.restaurant_menu, container, false);

            mLinearParent = (LinearLayout) this.mView.findViewById(R.id.menu_linear_details);
            mBtnDirections = (Button) this.mView.findViewById(R.id.btn_direction);
            mBtnDirections.setOnClickListener(onDirectionClicklistener);
            render();
        }
        return this.mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        getContext().registerReceiver(receiver, new IntentFilter(
                UDecideBackgroundService.UDecideBroadcaster));
    }

    @Override
    public void onPause() {
        super.onPause();
        getContext().unregisterReceiver(receiver);
    }

    protected void render() {
        renderDetails();
        getRestaurantMenu();
    }

    protected void renderDetails() {
        try {
            View details_view = LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.restaurant_menu_details, null);

            SupportMapFragment fragMap = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.menu_map);
            fragMap.getMapAsync(this);

            TextView mTxtRestaurantName = (TextView) details_view.findViewById(R.id.txt_menu_restoname);
            TextView mTxtRestaurantAddress = (TextView) details_view.findViewById(R.id.txt_menu_address);
            TextView mTxtRestaurantCategory = (TextView) details_view.findViewById(R.id.txt_menu_category);
            TextView mTxtRestaurantCategoryCuisine = (TextView) details_view.findViewById(R.id.txt_menu_category_cuisine);
            TextView mTxtRestaurantCategoryInfo = (TextView) details_view.findViewById(R.id.txt_menu_category_info);
            TextView mTxtRestaurantCategoryAddedInfo = (TextView) details_view.findViewById(R.id.txt_menu_category_added_info);
            TextView mTxtRestaurantHours = (TextView) details_view.findViewById(R.id.txt_menu_hours);
            TextView mTxtRestaurantReviewsCount = (TextView) details_view.findViewById(R.id.txt_menu_reviews_count);

            TextView mTxtCheckinMaxCapacity = (TextView) details_view.findViewById(R.id.checkin_max_capacity);
            TextView mTxtCheckinCheckinToday = (TextView) details_view.findViewById(R.id.checkin_count_today);
            TextView mTxtCheckinRemainingSeats = (TextView) details_view.findViewById(R.id.checkin_remaining_seats);
            TextView mTxtCheckinStatus = (TextView) details_view.findViewById(R.id.checkin_status);

            mTxtRestaurantDistance = (TextView) details_view.findViewById(R.id.txt_menu_distance);
            mTxtRestaurantDistance.setVisibility(View.GONE);
            RatingBar mRTB_ratings = (RatingBar) details_view.findViewById(R.id.rating_bar);
            LinearLayout mLinMenuThumbnails = (LinearLayout) details_view.findViewById(R.id.menu_thumbnails);
            RelativeLayout mReviewRateContainer = (RelativeLayout) details_view.findViewById(R.id.rate_reviews_container);
            mBtnCheckin = (Button) details_view.findViewById(R.id.btn_checkin);
            mBtnCheckin.setVisibility(View.GONE);
            mBtnCheckin.setOnClickListener(onCheckinListener);

            mBtnReserve = (Button) details_view.findViewById(R.id.btn_reserve);
            mBtnReserve.setOnClickListener(onReserveClicked);

            mTxtRestaurantName.setText(mRestaurant.getResto_name());
            mTxtRestaurantAddress.setText(mRestaurant.getAddress());
            mTxtRestaurantCategory.setText(mRestaurant.getCategory().getType());
            mTxtRestaurantCategoryCuisine.setText(mRestaurant.getCategory().getCuisine());
            mTxtRestaurantCategoryInfo.setText(mRestaurant.getCategory().getMore_info());
            mTxtRestaurantCategoryAddedInfo.setText(mRestaurant.getCategory().getAdded_info());
            mTxtRestaurantHours.setText("Hours today: " + mRestaurant.getStore_hours());

            mTxtCheckinMaxCapacity.setText(String.valueOf(mRestaurant.getMax_capacity()));
            mTxtCheckinCheckinToday.setText(String.valueOf(mRestaurant.getCheckins_today()));

            remainingSeat = mRestaurant.getMax_capacity() - mRestaurant.getCheckins_today();
            String checkin_status = (remainingSeat > 0) ? "Available" : "Full";
            mTxtCheckinStatus.setText(checkin_status);
            mTxtCheckinRemainingSeats.setText(String.valueOf(remainingSeat));

            if (mRestaurant.getRatings() > 0 && mRestaurant.getReviews_count() > 0) {
                mRTB_ratings.setRating((float) mRestaurant.getRatings());
                mRTB_ratings.invalidate();
                mTxtRestaurantReviewsCount.setText(mRestaurant.getReviews_count() + ((mRestaurant.getReviews_count() > 1) ? " reviews" : " review"));
            } else {
                mReviewRateContainer.setVisibility(View.GONE);
            }

            if (!mRestaurant.getStore_status().equalsIgnoreCase("closed") && remainingSeat > 0) {
                mBtnReserve.setVisibility(View.VISIBLE);
            } else {
                mBtnReserve.setVisibility(View.GONE);
            }

            //render thumbnails
            if (mRestaurant.getThumbnails() != null && mRestaurant.getThumbnails().size() > 0) {
                for (Thumbnail thumbnail : mRestaurant.getThumbnails()) {
                    ImageView img_thumbnail = new ImageView(getContext());
                    img_thumbnail.setTag(thumbnail);
                    img_thumbnail.setPadding(8, 8, 8, 8);

                    if (AsyncBitmapLoader.cancelPotentialWork(UDecideConstants.BASE_URL + thumbnail.getFile_path(), img_thumbnail)) {
                        AsyncBitmapLoader task = new AsyncBitmapLoader(getContext(), img_thumbnail, 400, 400);
                        AsyncBitmapLoader.AsyncDrawable asyncDrawable =
                                new AsyncBitmapLoader.AsyncDrawable(getResources(), UDecideConstants.getDefaultImagePlaceholder(getContext()), task);
                        img_thumbnail.setImageDrawable(asyncDrawable);
                        task.execute(UDecideConstants.BASE_URL + thumbnail.getFile_path());
                    }

                    img_thumbnail.setOnLongClickListener(thumbnail_listener);
                    mLinMenuThumbnails.addView(img_thumbnail);
                }
            }
            mLinearParent.addView(details_view);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("TEST", "[ERROR][renderDetails] |-> " + e.getMessage());
        }
    }

    protected View.OnClickListener onReserveClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            seatReservation();
        }
    };

    private void seatReservation() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
        View customView = LayoutInflater.from(getContext()).inflate(R.layout.reservation_modal, null);
        mBuilder.setView(customView);

        final EditText etxtNumSeat = (EditText) customView.findViewById(R.id.reservation_num_seat);
        final EditText etxtRequest = (EditText) customView.findViewById(R.id.reservation_request);
        final DatePicker dtPicker = (DatePicker) customView.findViewById(R.id.date_picker);
        final TimePicker tmPicker = (TimePicker) customView.findViewById(R.id.time_picker);

        mBuilder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                if (etxtNumSeat.getText().toString().isEmpty()) {
                    ToastHandler.ShowToast(getContext(), "Number of seat must have a value.");
                    return;
                }

                int day = dtPicker.getDayOfMonth();
                int month = dtPicker.getMonth();
                int year = dtPicker.getYear() - 1900;

                int hour = tmPicker.getCurrentHour();
                int minute = tmPicker.getCurrentMinute();

                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                Date d = new Date(year, month, day);
                String strDate = dateFormatter.format(d);
                String strReservationDate = strDate + " " + hour + ":" + minute + ":00";

                Log.d("RESERVE", "[onReserveClicked][date]|-> " + strReservationDate);
                try {
                    JSONObject objParams = new JSONObject();
                    objParams.put("user_id", new UDecideHelper().getUserId(getContext()));
                    objParams.put("restaurant_id", mRestaurant.getId());
                    objParams.put("num_seat", etxtNumSeat.getText().toString());
                    objParams.put("reservation_date", strReservationDate);
                    objParams.put("additional_request", etxtRequest.getText().toString());

                    String postUrl = UDecideConstants.BASE_URL + "api/restaurant/seat/reservation";
                    new HttpClientHelper().executeInBackground(getContext(), postUrl, objParams, null, true, true, new ArrayList<HEADERS>(), new HttpClientInterface() {
                        ProgressDialog prgdlg;

                        @Override
                        public void onBeforeExecute(int i) {
                            prgdlg = ProgressDialog.show(getActivity(), "", "loading....");
                        }

                        @Override
                        public void onHttpClientResponse(int i, String s) {
                            prgdlg.setMessage("please wait....");
                            Log.d("RESERVE", "[onReserveClicked][onHttpClientResponse]|-> " + s);
                            try {
                                JSONObject json = new JSONObject(s);
                                ToastHandler.ShowToast(getContext(), json.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onHttpClientErrorException(int i, Exception e) {
                            prgdlg.setMessage(e.getMessage());
                            prgdlg.dismiss();
                            Log.d("RESERVE", "[onReserveClicked][onHttpClientErrorException]|-> " + e.getMessage());
                        }

                        @Override
                        public void onAfterExecute(int i) {
                            prgdlg.dismiss();
                            dialog.dismiss();
                        }
                    }, 10101);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        mBuilder.create().show();
    }

    protected void renderRestaurantReviews() {
        Log.d("TEST", "REVIEWS COUNT |-> " + mRestaurant.getReviews().size());

        try {
            // divider
            CustomImageDivider imgDivider = new CustomImageDivider(getContext());
            mLinearParent.addView(imgDivider);

            View review_view_header = LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.review_container_header, null);
            Button btnRateNow = (Button) review_view_header.findViewById(R.id.btn_submit_review);
            if (!mRestaurant.is_rated()) {
                btnRateNow.setTag(mRestaurant);
                btnRateNow.setOnClickListener(onReviewClickListener);
            } else {
                btnRateNow.setVisibility(View.GONE);
            }
            mLinearParent.addView(review_view_header);

            for (Restaurant.Reviews review : mRestaurant.getReviews()) {
                View review_view = LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.review_container, null);

                ImageView imgAvatar = (ImageView) review_view.findViewById(R.id.review_avatar);
                TextView txtAvatarUsername = (TextView) review_view.findViewById(R.id.review_avatar_username);
                RatingBar rtReviews = (RatingBar) review_view.findViewById(R.id.review_ratings);
                TextView txtContent = (TextView) review_view.findViewById(R.id.review_content);
                TextView txtDate = (TextView) review_view.findViewById(R.id.review_date);

                if (AsyncBitmapLoader.cancelPotentialWork(UDecideConstants.BASE_URL + review.getAvatar(), imgAvatar)) {
                    AsyncBitmapLoader task = new AsyncBitmapLoader(getContext(), imgAvatar, 100, 100);
                    AsyncBitmapLoader.AsyncDrawable asyncDrawable =
                            new AsyncBitmapLoader.AsyncDrawable(getResources(), UDecideConstants.getDefaultImagePlaceholder(getContext()), task);
                    imgAvatar.setImageDrawable(asyncDrawable);
                    task.execute(UDecideConstants.BASE_URL + review.getAvatar());
                }
                txtAvatarUsername.setText(review.getUsername());
                txtContent.setText(review.getContent());
                txtDate.setText(review.getCreated_at());
                rtReviews.setRating((float) review.getRatings());
                rtReviews.invalidate();

                mLinearParent.addView(review_view);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("TEST", "[ERROR][renderRestaurantReviews] |-> " + e.getMessage());
        }
    }

    protected View.OnClickListener onReviewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final Restaurant restaurant = (Restaurant) v.getTag();

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
            LayoutInflater inflater = LayoutInflater.from(getActivity().getApplicationContext());
            View dialogView = inflater.inflate(R.layout.review_modal, null);
            dialogBuilder.setView(dialogView);

            final RatingBar rtBar = (RatingBar) dialogView.findViewById(R.id.review_ratings);
            final EditText rvw_content = (EditText) dialogView.findViewById(R.id.review_content);
            Button btn_submit_review = (Button) dialogView.findViewById(R.id.btn_submit_review);
            final AlertDialog alertDialog = dialogBuilder.create();
            btn_submit_review.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (rvw_content.getText().length() > 0 && rtBar.getRating() > 0) {
                        submitReview(restaurant.getId(), rvw_content.getText().toString(), rtBar.getRating(), alertDialog);
                    } else {
                        ToastHandler.ShowToast(getContext(), "Fill in all fields.");
                    }
                }
            });
            alertDialog.show();
        }
    };

    public View.OnClickListener onDirectionClicklistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mCurrentLocation != null && mRestaurant != null && mGoogleMap != null) {
                LatLng origin = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
                LatLng destination = new LatLng(mRestaurant.getLocation().getLatitude(), mRestaurant.getLocation().getLongitude());

                mRouteDirections.findDirection(origin, destination, "You", mRestaurant.getResto_name(), null, null);
            }
        }
    };

    protected void submitReview(int link_id, String content, float ratings, final AlertDialog dlg) {
        try {
            JSONObject objReg = new JSONObject();
            objReg.put("user_id", new UDecideHelper().getUserId(getContext()));
            objReg.put("link_id", link_id);
            objReg.put("module_type", "restaurant");
            objReg.put("content", content);
            objReg.put("ratings", ratings);

            String submitUrl = UDecideConstants.BASE_URL + "api/reviews/add";
            new HttpClientHelper().executeInBackground(getContext(), submitUrl, objReg, null, true, true, new ArrayList<HEADERS>(), new HttpClientInterface() {
                ProgressDialog loader;

                @Override
                public void onBeforeExecute(int i) {
                    Log.d("TEST", "(onBeforeExecute) i - value -> " + i);
                    loader = ProgressDialog.show(getActivity(), "", "loading....");
                }

                @Override
                public void onHttpClientResponse(int i, String s) {
                    loader.setMessage("please wait....");
                    Log.d("TEST", "(onHttpClientResponse) i - value -> " + i);
                    Log.d("TEST", "(onHttpClientResponse) String s -> " + s);
                }

                @Override
                public void onHttpClientErrorException(int i, Exception e) {
                    Log.d("TEST", "(onHttpClientErrorException) i - value -> " + i);
                    Log.d("TEST", "(onHttpClientErrorException) i - msg -> " + e.toString());
                    ToastHandler.ShowToast(getActivity(), e.toString());
                    loader.setMessage(e.getMessage());
                    loader.dismiss();
                    dlg.dismiss();

                    // back to client dashboard activity
                    getActivity().getSupportFragmentManager().beginTransaction().remove(FragmentRestaurantMenu.this).commit();
                    getActivity().getSupportFragmentManager().popBackStack();
                }

                @Override
                public void onAfterExecute(int i) {
                    Log.d("TEST", "(onAfterExecute) i - value -> " + i);
                    loader.dismiss();
                    dlg.dismiss();
                    ToastHandler.ShowToast(getContext(), "Completed.");

                    refreshRestaurant();
                }
            }, 10002);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("TEST", "ERROR[submitRegistration] -> " + e.getMessage());
        }
    }

    protected View.OnClickListener onCheckinListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());

            mBuilder.setMessage("Do you really want to check-in?");
            mBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialogInterface, int i) {
                    try {
                        JSONObject objReg = new JSONObject();
                        objReg.put("user_id", new UDecideHelper().getUserId(getContext()));
                        objReg.put("restaurant_id", mRestaurant.getId());

                        String submitUrl = UDecideConstants.BASE_URL + "api/checkins/add";
                        new HttpClientHelper().executeInBackground(getContext(), submitUrl, objReg, null, true, true, new ArrayList<HEADERS>(), new HttpClientInterface() {
                            ProgressDialog loader;

                            @Override
                            public void onBeforeExecute(int i) {
                                Log.d("TEST", "(onBeforeExecute) i - value -> " + i);
                                loader = ProgressDialog.show(getActivity(), "", "loading....");
                            }

                            @Override
                            public void onHttpClientResponse(int i, String s) {
                                loader.setMessage("please wait....");
                                Log.d("TEST", "(onHttpClientResponse) i - value -> " + i);
                                Log.d("TEST", "(onHttpClientResponse) String s -> " + s);
                            }

                            @Override
                            public void onHttpClientErrorException(int i, Exception e) {
                                Log.d("TEST", "(onHttpClientErrorException) i - value -> " + i);
                                Log.d("TEST", "(onHttpClientErrorException) i - msg -> " + e.toString());
                                ToastHandler.ShowToast(getActivity(), e.toString());
                                loader.setMessage(e.getMessage());
                                loader.dismiss();
                                dialogInterface.dismiss();

                                // back to client dashboard activity
                                getActivity().getSupportFragmentManager().beginTransaction().remove(FragmentRestaurantMenu.this).commit();
                                getActivity().getSupportFragmentManager().popBackStack();
                            }

                            @Override
                            public void onAfterExecute(int i) {
                                Log.d("TEST", "(onAfterExecute) i - value -> " + i);
                                loader.dismiss();
                                dialogInterface.dismiss();
                                ToastHandler.ShowToast(getContext(), "Completed.");

                                refreshRestaurant();
                            }
                        }, 10002);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        ToastHandler.ShowToast(getContext(), "[ERROR] |-> " + e.getMessage());
                    }
                }
            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            mBuilder.create().show();
        }
    };

    protected void renderMapLocation(LatLng latlon) {
        if (mGoogleMap == null) {
            renderMapLocation(latlon);
        }

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latlon);
        markerOptions.title("You");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
//        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.app_icon));
        mGoogleMap.addMarker(markerOptions);

        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latlon));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }

    protected void renderMenuList(ArrayList<RestaurantMenu> menus) {
        try {
            // divider
            CustomImageDivider imgDivider = new CustomImageDivider(getContext());
            mLinearParent.addView(imgDivider);

            RelativeLayout layout_menulist_label = new RelativeLayout(getContext());
            RelativeLayout.LayoutParams lbl_params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layout_menulist_label.setPadding(0, 20, 0, 10);
            layout_menulist_label.setLayoutParams(lbl_params);
            RelativeLayout.LayoutParams txtlbl_params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            txtlbl_params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            TextView lblMenuList = new TextView(getContext());
            lblMenuList.setTextColor(Color.parseColor("#000000"));
            lblMenuList.setLayoutParams(txtlbl_params);
            lblMenuList.setText("Menu List");
            lblMenuList.setTextSize(20);
            lblMenuList.setTypeface(Typeface.DEFAULT_BOLD);
            layout_menulist_label.addView(lblMenuList);
            mLinearParent.addView(layout_menulist_label);
            for (RestaurantMenu menu : menus) {
                LinearLayout mLinearMainContainer = new LinearLayout(getContext());
                mLinearMainContainer.setOrientation(LinearLayout.VERTICAL);
                mLinearMainContainer.setBackgroundColor(Color.parseColor("#ebebe0"));
                mLinearMainContainer.setPadding(10, 10, 10, 10);
                LinearLayout.LayoutParams mLinearMainContainer_params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mLinearMainContainer_params.setMargins(0, 10, 0, 10);
                mLinearMainContainer.setLayoutParams(mLinearMainContainer_params);

                TextView txtFoodName = new TextView(getContext());
                txtFoodName.setTextSize(20);
                txtFoodName.setTypeface(Typeface.DEFAULT_BOLD);
                txtFoodName.setTextColor(Color.parseColor("#000000"));
                txtFoodName.setText(menu.getFood_name());

                TextView txtDescription = new TextView(getContext());
                txtDescription.setTextSize(14);
                txtDescription.setTextColor(Color.parseColor("#000000"));
                txtDescription.setText(menu.getDescription());

                TextView txtMenutype = new TextView(getContext());
                txtMenutype.setTextSize(14);
                txtMenutype.setTextColor(Color.parseColor("#000000"));
                txtMenutype.setText(menu.getMenu_type());

                TextView txtPrice = new TextView(getContext());
                txtPrice.setTextSize(15);
                txtPrice.setTypeface(Typeface.DEFAULT_BOLD);
                txtPrice.setTextColor(Color.parseColor("#000000"));
                txtPrice.setText("₱ " + String.valueOf(menu.getPrice()));

                TextView txtPrepTime = new TextView(getContext());
                txtPrepTime.setTextSize(14);
                txtPrepTime.setTextColor(Color.parseColor("#000000"));
                txtPrepTime.setText(String.valueOf(menu.getPreparation_time()) + " minute(s) to serve.");

                // menu thumbnails
                HorizontalScrollView horScrView = new HorizontalScrollView(getContext());
                LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                horScrView.setLayoutParams(lParams);

                LinearLayout mLinMenu_Thumbnails = new LinearLayout(getContext());
                mLinMenu_Thumbnails.setPadding(5, 5, 5, 5);
                mLinMenu_Thumbnails.setOrientation(LinearLayout.HORIZONTAL);
                mLinMenu_Thumbnails.setWeightSum(1);
                LinearLayout.LayoutParams lParams2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                mLinMenu_Thumbnails.setLayoutParams(lParams2);
                horScrView.addView(mLinMenu_Thumbnails);
                for (Thumbnail thumbnail : menu.getThumbnails()) {
                    ImageView img_thumbnail = new ImageView(getContext());
                    img_thumbnail.setTag(thumbnail);
                    img_thumbnail.setPadding(8, 8, 8, 8);

                    if (AsyncBitmapLoader.cancelPotentialWork(UDecideConstants.BASE_URL + thumbnail.getFile_path(), img_thumbnail)) {
                        AsyncBitmapLoader task = new AsyncBitmapLoader(getContext(), img_thumbnail, 500, 500);
                        AsyncBitmapLoader.AsyncDrawable asyncDrawable =
                                new AsyncBitmapLoader.AsyncDrawable(getResources(), UDecideConstants.getDefaultImagePlaceholder(getContext()), task);
                        img_thumbnail.setImageDrawable(asyncDrawable);
                        task.execute(UDecideConstants.BASE_URL + thumbnail.getFile_path());
                    }

                    img_thumbnail.setOnLongClickListener(thumbnail_listener);
                    mLinMenu_Thumbnails.addView(img_thumbnail);
                }

                // add views to menu container
                mLinearMainContainer.addView(txtFoodName);
                mLinearMainContainer.addView(txtDescription);
                mLinearMainContainer.addView(txtMenutype);
                mLinearMainContainer.addView(horScrView);
                mLinearMainContainer.addView(txtPrice);
                mLinearMainContainer.addView(txtPrepTime);

                // add to parent container
                mLinearParent.addView(mLinearMainContainer);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("TEST", "[ERROR][renderMenuList] |-> " + e.getMessage());
        }
    }

    protected void getRestaurantMenu() {
        if (mRestaurant == null) {
            getRestaurantMenu();
        }
        HEADERS headers = new HEADERS();
        headers.addHeaders("UDecide_USERID", new UDecideHelper().getUserId(getContext()));
        String menu_url = UDecideConstants.BASE_URL + "api/restaurant/" + mRestaurant.getId() + "/menus";
        new HttpClientHelper().executeInBackground(getContext(), menu_url, null, null, false, false, headers.getArrHeaders(), this, 1000);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    protected ImageView.OnLongClickListener thumbnail_listener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            String thumbnail_path = null;
            Thumbnail thumbnail = (Thumbnail) ((ImageView) v).getTag();
            thumbnail_path = thumbnail.getFile_path();
            if (thumbnail_path != null) {
                showImagePreview(thumbnail_path);
            }
            return false;
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);

        mRouteDirections = new MapRouteDirections(getActivity().getApplicationContext(), mGoogleMap);

        final LatLng latlon = new LatLng(mRestaurant.getLocation().getLatitude(), mRestaurant.getLocation().getLongitude());
        renderMapLocation(latlon);
    }

    @Override
    public void onBeforeExecute(int i) {

    }

    @Override
    public void onHttpClientResponse(int i, String s) {
        renderMenuList(new RestaurantParser().parseRestaurantMenu(s));
    }

    @Override
    public void onHttpClientErrorException(int i, Exception e) {
        ToastHandler.ShowToast(getContext(), e.toString());
    }

    @Override
    public void onAfterExecute(int i) {
        renderRestaurantReviews();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location location = (Location) intent.getExtras().getParcelable("location_listener");
            if (location != null) {
                Log.d("TEST", "[BroadcastReceiver][location]");
                setServiceResponseLocation(location);
            }
        }
    };

    private void setServiceResponseLocation(final Location location) {
        mCurrentLocation = location;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mTxtRestaurantDistance.setVisibility(View.VISIBLE);
                double distance = new UDecideHelper().getDistanceBetweenLocation(location.getLatitude(), location.getLongitude(), mRestaurant.getLocation().getLatitude(), mRestaurant.getLocation().getLongitude());
                mTxtRestaurantDistance.setText(String.format("%.2f", distance) + " km(s) away");

                if (mBtnCheckin != null) {
                    if (distance <= UDecideConstants.CHECKOUT_DISTANCE) {
                        if (!mRestaurant.is_checked_in()) {
                            mBtnCheckin.setVisibility(View.VISIBLE);
                        } else {
                            mBtnCheckin.setVisibility(View.GONE);
                        }
                    } else {
                        mBtnCheckin.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);

    }

    protected void showImagePreview(String image_path) {
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //nothing;
            }
        });

        CustomImageView imageView = new CustomImageView(getContext());

        if (AsyncBitmapLoader.cancelPotentialWork(UDecideConstants.BASE_URL + image_path, imageView)) {
            AsyncBitmapLoader task = new AsyncBitmapLoader(getContext(), imageView, 0, 0);
            AsyncBitmapLoader.AsyncDrawable asyncDrawable =
                    new AsyncBitmapLoader.AsyncDrawable(getResources(), UDecideConstants.getDefaultImagePlaceholder(getContext()), task);
            imageView.setImageDrawable(asyncDrawable);
            task.execute(UDecideConstants.BASE_URL + image_path);
        }

        RelativeLayout.LayoutParams lParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        builder.addContentView(imageView, lParams);
        builder.show();
    }

    protected void refreshRestaurant() {
        HEADERS headers = new HEADERS();
        headers.addHeaders("UDecide_USERID", new UDecideHelper().getUserId(getContext()));
        String restaurantURL = UDecideConstants.BASE_URL + "api/restaurant/" + mRestaurant.getId();
        new HttpClientHelper().executeInBackground(getActivity(), restaurantURL, null, null, false, false, headers.getArrHeaders(), new HttpClientInterface() {
            ProgressDialog loader;

            @Override
            public void onBeforeExecute(int i) {
                Log.d("TEST", "[refreshRestaurant](onBeforeExecute) i - value -> " + i);
                loader = ProgressDialog.show(getActivity(), "", "loading....");
            }

            @Override
            public void onHttpClientResponse(int i, String s) {
                loader.setMessage("please wait....");
                Log.d("TEST", "[refreshRestaurant](onHttpClientResponse) i - value -> " + i);
                Log.d("TEST", "[refreshRestaurant](onHttpClientResponse) String s -> " + s);
                try {
                    mRestaurant = new RestaurantParser().parseRestaurantSolo(s);
                    mLinearParent.removeAllViews();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("TEST", "[ERROR][refreshRestaurant](onHttpClientResponse) i - value -> " + i);
                    Log.d("TEST", "[ERROR][refreshRestaurant](onHttpClientResponse) String s -> " + s);
                    loader.setMessage(e.getMessage());
                    loader.dismiss();
                }
            }

            @Override
            public void onHttpClientErrorException(int i, Exception e) {
                Log.d("TEST", "[refreshRestaurant](onHttpClientErrorException) i - value -> " + i);
                Log.d("TEST", "[refreshRestaurant](onHttpClientErrorException) i - msg -> " + e.toString());
                ToastHandler.ShowToast(getActivity(), e.toString());
                loader.setMessage(e.getMessage());
                loader.dismiss();

                // back to client dashboard activity
                getActivity().getSupportFragmentManager().beginTransaction().remove(FragmentRestaurantMenu.this).commit();
                getActivity().getSupportFragmentManager().popBackStack();
            }

            @Override
            public void onAfterExecute(int i) {
                Log.d("TEST", "[refreshRestaurant](onAfterExecute) i - value -> " + i);
                loader.dismiss();
                ToastHandler.ShowToast(getContext(), "Completed.");

                render();
            }
        }, 10101);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem itm = menu.findItem(R.id.action_advance_order);
        itm.setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_advance_order) {
            Fragment mFragment = new FragmentAdvanceOrder();

            Bundle bundle = new Bundle();
            bundle.putParcelable("restaurant", mRestaurant);
            mFragment.setArguments(bundle);

            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frag_container, mFragment, "restaurant")
                    .addToBackStack("advance_order")
                    .commit();
        }
        return super.onOptionsItemSelected(item);
    }
}

package org.app.udecide.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class AdvanceOrder implements Parcelable {

    private int id;
    private String ref_code;
    private int user_id;
    private int restaurant_id;
    private String restaurant_name;
    private double total_amount;
    private int status;
    private String created_at;
    private String updated_at;
    private ArrayList<AdvanceOrderMenus> orders = new ArrayList<AdvanceOrderMenus>();
    private Delivery delivery;
    private String expectedTime;

    public AdvanceOrder() {
    }

    public AdvanceOrder(int id, String ref_code, int user_id, int restaurant_id, String restaurant_name, double total_amount, int status, String created_at, String updated_at, ArrayList<AdvanceOrderMenus> orders, Delivery delivery, String expectedTime) {
        this.id = id;
        this.ref_code = ref_code;
        this.user_id = user_id;
        this.restaurant_id = restaurant_id;
        this.restaurant_name = restaurant_name;
        this.total_amount = total_amount;
        this.status = status;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.orders = orders;
        this.delivery = delivery;
        this.expectedTime = expectedTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRef_code() {
        return ref_code;
    }

    public void setRef_code(String ref_code) {
        this.ref_code = ref_code;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(int restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public double getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(double total_amount) {
        this.total_amount = total_amount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public ArrayList<AdvanceOrderMenus> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<AdvanceOrderMenus> orders) {
        this.orders = orders;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public void setExpectedTime(String expectedTime) {
        this.expectedTime = expectedTime;
    }

    public String getExpectedTime() {
        return expectedTime;
    }

    public static class AdvanceOrderMenus implements Parcelable {

        private int advance_order_id;
        private int menu_id;
        private int item_count;
        private String created_at;
        private String updated_at;
        private int id;
        private int restaurant_id;
        private String food_name;
        private String description;
        private String menu_type;
        private double price;
        private double preparation_time;
        private ArrayList<Thumbnail> thumbnails = new ArrayList<Thumbnail>();

        public AdvanceOrderMenus() {
        }

        public AdvanceOrderMenus(int advance_order_id, int menu_id, int item_count, String created_at, String updated_at, int id, int restaurant_id, String food_name, String description, String menu_type, double price, double preparation_time, ArrayList<Thumbnail> thumbnails) {
            this.advance_order_id = advance_order_id;
            this.menu_id = menu_id;
            this.item_count = item_count;
            this.created_at = created_at;
            this.updated_at = updated_at;
            this.id = id;
            this.restaurant_id = restaurant_id;
            this.food_name = food_name;
            this.description = description;
            this.menu_type = menu_type;
            this.price = price;
            this.preparation_time = preparation_time;
            this.thumbnails = thumbnails;
        }

        public int getAdvance_order_id() {
            return advance_order_id;
        }

        public void setAdvance_order_id(int advance_order_id) {
            this.advance_order_id = advance_order_id;
        }

        public int getMenu_id() {
            return menu_id;
        }

        public void setMenu_id(int menu_id) {
            this.menu_id = menu_id;
        }

        public int getItem_count() {
            return item_count;
        }

        public void setItem_count(int item_count) {
            this.item_count = item_count;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getRestaurant_id() {
            return restaurant_id;
        }

        public void setRestaurant_id(int restaurant_id) {
            this.restaurant_id = restaurant_id;
        }

        public String getFood_name() {
            return food_name;
        }

        public void setFood_name(String food_name) {
            this.food_name = food_name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getMenu_type() {
            return menu_type;
        }

        public void setMenu_type(String menu_type) {
            this.menu_type = menu_type;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public double getPreparation_time() {
            return preparation_time;
        }

        public void setPreparation_time(double preparation_time) {
            this.preparation_time = preparation_time;
        }

        public ArrayList<Thumbnail> getThumbnails() {
            return thumbnails;
        }

        public void setThumbnails(ArrayList<Thumbnail> thumbnails) {
            this.thumbnails = thumbnails;
        }

        protected AdvanceOrderMenus(Parcel in) {
            advance_order_id = in.readInt();
            menu_id = in.readInt();
            item_count = in.readInt();
            created_at = in.readString();
            updated_at = in.readString();
            id = in.readInt();
            restaurant_id = in.readInt();
            food_name = in.readString();
            description = in.readString();
            menu_type = in.readString();
            price = in.readDouble();
            preparation_time = in.readDouble();
            if (in.readByte() == 0x01) {
                thumbnails = new ArrayList<Thumbnail>();
                in.readList(thumbnails, Thumbnail.class.getClassLoader());
            } else {
                thumbnails = null;
            }
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(advance_order_id);
            dest.writeInt(menu_id);
            dest.writeInt(item_count);
            dest.writeString(created_at);
            dest.writeString(updated_at);
            dest.writeInt(id);
            dest.writeInt(restaurant_id);
            dest.writeString(food_name);
            dest.writeString(description);
            dest.writeString(menu_type);
            dest.writeDouble(price);
            dest.writeDouble(preparation_time);
            if (thumbnails == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(thumbnails);
            }
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<AdvanceOrderMenus> CREATOR = new Parcelable.Creator<AdvanceOrderMenus>() {
            @Override
            public AdvanceOrderMenus createFromParcel(Parcel in) {
                return new AdvanceOrderMenus(in);
            }

            @Override
            public AdvanceOrderMenus[] newArray(int size) {
                return new AdvanceOrderMenus[size];
            }
        };
    }

    public static class Delivery implements Parcelable {
        private int id;
        private int ao_id;
        private String ref_code;
        private int restaurant_id;
        private int foodie_id;
        private String address;
        private String contact_number;
        private int status;
        private String remarks;
        private String delivery_name;
        private int updated_by;
        private String created_at;
        private String updated_at;
        private int user_id;
        private String name;
        private String contact_no;
        private String gender;
        private String email;
        private String delivery_address;

        public Delivery() {
        }

        public Delivery(int id, int ao_id, String ref_code, int restaurant_id, int foodie_id, String address, String contact_number, int status, String remarks, String delivery_name, int updated_by, String created_at, String updated_at, int user_id, String name, String contact_no, String gender, String email, String delivery_address) {
            this.id = id;
            this.ao_id = ao_id;
            this.ref_code = ref_code;
            this.restaurant_id = restaurant_id;
            this.foodie_id = foodie_id;
            this.address = address;
            this.contact_number = contact_number;
            this.status = status;
            this.remarks = remarks;
            this.delivery_name = delivery_name;
            this.updated_by = updated_by;
            this.created_at = created_at;
            this.updated_at = updated_at;
            this.user_id = user_id;
            this.name = name;
            this.contact_no = contact_no;
            this.gender = gender;
            this.email = email;
            this.delivery_address = delivery_address;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getAo_id() {
            return ao_id;
        }

        public void setAo_id(int ao_id) {
            this.ao_id = ao_id;
        }

        public String getRef_code() {
            return ref_code;
        }

        public void setRef_code(String ref_code) {
            this.ref_code = ref_code;
        }

        public int getRestaurant_id() {
            return restaurant_id;
        }

        public void setRestaurant_id(int restaurant_id) {
            this.restaurant_id = restaurant_id;
        }

        public int getFoodie_id() {
            return foodie_id;
        }

        public void setFoodie_id(int foodie_id) {
            this.foodie_id = foodie_id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getContact_number() {
            return contact_number;
        }

        public void setContact_number(String contact_number) {
            this.contact_number = contact_number;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getDelivery_name() {
            return delivery_name;
        }

        public void setDelivery_name(String delivery_name) {
            this.delivery_name = delivery_name;
        }

        public int getUpdated_by() {
            return updated_by;
        }

        public void setUpdated_by(int updated_by) {
            this.updated_by = updated_by;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getContact_no() {
            return contact_no;
        }

        public void setContact_no(String contact_no) {
            this.contact_no = contact_no;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getDelivery_address() {
            return delivery_address;
        }

        public void setDelivery_address(String delivery_address) {
            this.delivery_address = delivery_address;
        }

        protected Delivery(Parcel in) {
            id = in.readInt();
            ao_id = in.readInt();
            ref_code = in.readString();
            restaurant_id = in.readInt();
            foodie_id = in.readInt();
            address = in.readString();
            contact_number = in.readString();
            status = in.readInt();
            remarks = in.readString();
            delivery_name = in.readString();
            updated_by = in.readInt();
            created_at = in.readString();
            updated_at = in.readString();
            user_id = in.readInt();
            name = in.readString();
            contact_no = in.readString();
            gender = in.readString();
            email = in.readString();
            delivery_address = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeInt(ao_id);
            dest.writeString(ref_code);
            dest.writeInt(restaurant_id);
            dest.writeInt(foodie_id);
            dest.writeString(address);
            dest.writeString(contact_number);
            dest.writeInt(status);
            dest.writeString(remarks);
            dest.writeString(delivery_name);
            dest.writeInt(updated_by);
            dest.writeString(created_at);
            dest.writeString(updated_at);
            dest.writeInt(user_id);
            dest.writeString(name);
            dest.writeString(contact_no);
            dest.writeString(gender);
            dest.writeString(email);
            dest.writeString(delivery_address);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Delivery> CREATOR = new Parcelable.Creator<Delivery>() {
            @Override
            public Delivery createFromParcel(Parcel in) {
                return new Delivery(in);
            }

            @Override
            public Delivery[] newArray(int size) {
                return new Delivery[size];
            }
        };
    }


    protected AdvanceOrder(Parcel in) {
        id = in.readInt();
        ref_code = in.readString();
        user_id = in.readInt();
        restaurant_id = in.readInt();
        restaurant_name = in.readString();
        total_amount = in.readDouble();
        status = in.readInt();
        created_at = in.readString();
        updated_at = in.readString();
        if (in.readByte() == 0x01) {
            orders = new ArrayList<AdvanceOrderMenus>();
            in.readList(orders, AdvanceOrderMenus.class.getClassLoader());
        } else {
            orders = null;
        }
        delivery = (Delivery) in.readValue(Delivery.class.getClassLoader());
        expectedTime = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(ref_code);
        dest.writeInt(user_id);
        dest.writeInt(restaurant_id);
        dest.writeString(restaurant_name);
        dest.writeDouble(total_amount);
        dest.writeInt(status);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        if (orders == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(orders);
        }
        dest.writeValue(delivery);
        dest.writeString(expectedTime);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AdvanceOrder> CREATOR = new Parcelable.Creator<AdvanceOrder>() {
        @Override
        public AdvanceOrder createFromParcel(Parcel in) {
            return new AdvanceOrder(in);
        }

        @Override
        public AdvanceOrder[] newArray(int size) {
            return new AdvanceOrder[size];
        }
    };
}
package org.app.udecide.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Thumbnail implements Parcelable {

    private int id;
    private String file_path;
    private String module_type;
    private int link_id;
    private int is_primary;
    private int is_thumbnail;
    private String created_at;
    private String updated_at;

    public Thumbnail() {
    }

    public Thumbnail(int id, String file_path, String module_type, int link_id, int is_primary, int is_thumbnail, String created_at, String updated_at) {
        this.id = id;
        this.file_path = file_path;
        this.module_type = module_type;
        this.link_id = link_id;
        this.is_primary = is_primary;
        this.is_thumbnail = is_thumbnail;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getModule_type() {
        return module_type;
    }

    public void setModule_type(String module_type) {
        this.module_type = module_type;
    }

    public int getLink_id() {
        return link_id;
    }

    public void setLink_id(int link_id) {
        this.link_id = link_id;
    }

    public int getIs_primary() {
        return is_primary;
    }

    public void setIs_primary(int is_primary) {
        this.is_primary = is_primary;
    }

    public int getIs_thumbnail() {
        return is_thumbnail;
    }

    public void setIs_thumbnail(int is_thumbnail) {
        this.is_thumbnail = is_thumbnail;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    protected Thumbnail(Parcel in) {
        id = in.readInt();
        file_path = in.readString();
        module_type = in.readString();
        link_id = in.readInt();
        is_primary = in.readInt();
        is_thumbnail = in.readInt();
        created_at = in.readString();
        updated_at = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(file_path);
        dest.writeString(module_type);
        dest.writeInt(link_id);
        dest.writeInt(is_primary);
        dest.writeInt(is_thumbnail);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Thumbnail> CREATOR = new Parcelable.Creator<Thumbnail>() {
        @Override
        public Thumbnail createFromParcel(Parcel in) {
            return new Thumbnail(in);
        }

        @Override
        public Thumbnail[] newArray(int size) {
            return new Thumbnail[size];
        }
    };
}
package org.app.udecide.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Restaurant implements Parcelable {

    private int id;
    private int user_id;
    private int location_id;
    private String resto_name;
    private String owner_name;
    private String email;
    private String description;
    private int category_id;
    private String address;
    private String store_hours;
    private String contact_no;
    private int activation_status;
    private String created_at;
    private String updated_at;
    private Thumbnail thumbnail;
    private Location location;
    private Category category;
    private boolean is_rated;
    private double ratings;
    private int reviews_count;
    private int checkins_today;
    private int total_checkins;
    private boolean is_checked_in;
    private boolean has_delivery;
    private int max_capacity;
    private String store_status;
    private ArrayList<Thumbnail> thumbnails = new ArrayList<Thumbnail>();
    private ArrayList<Reviews> reviews = new ArrayList<Reviews>();

    public Restaurant() {
    }

    public Restaurant(int id, int user_id, int location_id, String resto_name, String owner_name, String email, String description, int category_id, String address, String store_hours, String contact_no, int activation_status, String created_at, String updated_at, Thumbnail thumbnail, Location location, Category category, boolean is_rated, double ratings, int reviews_count, int checkins_today, int total_checkins, boolean is_checked_in, boolean has_delivery, int max_capacity, String store_status, ArrayList<Thumbnail> thumbnails, ArrayList<Reviews> reviews) {

        this.id = id;
        this.user_id = user_id;
        this.location_id = location_id;
        this.resto_name = resto_name;
        this.owner_name = owner_name;
        this.email = email;
        this.description = description;
        this.category_id = category_id;
        this.address = address;
        this.store_hours = store_hours;
        this.contact_no = contact_no;
        this.activation_status = activation_status;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.thumbnail = thumbnail;
        this.location = location;
        this.category = category;
        this.is_rated = is_rated;
        this.ratings = ratings;
        this.reviews_count = reviews_count;
        this.checkins_today = checkins_today;
        this.total_checkins = total_checkins;
        this.is_checked_in = is_checked_in;
        this.has_delivery = has_delivery;
        this.max_capacity = max_capacity;
        this.store_status = store_status;
        this.thumbnails = thumbnails;
        this.reviews = reviews;
    }

    public int getId() {
        return id;

    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public String getResto_name() {
        return resto_name;
    }

    public void setResto_name(String resto_name) {
        this.resto_name = resto_name;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStore_hours() {
        return store_hours;
    }

    public void setStore_hours(String store_hours) {
        this.store_hours = store_hours;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public int getActivation_status() {
        return activation_status;
    }

    public void setActivation_status(int activation_status) {
        this.activation_status = activation_status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean is_rated() {
        return is_rated;
    }

    public void setIs_rated(boolean is_rated) {
        this.is_rated = is_rated;
    }

    public double getRatings() {
        return ratings;
    }

    public void setRatings(double ratings) {
        this.ratings = ratings;
    }

    public int getReviews_count() {
        return reviews_count;
    }

    public void setReviews_count(int reviews_count) {
        this.reviews_count = reviews_count;
    }

    public int getCheckins_today() {
        return checkins_today;
    }

    public void setCheckins_today(int checkins_today) {
        this.checkins_today = checkins_today;
    }

    public int getTotal_checkins() {
        return total_checkins;
    }

    public void setTotal_checkins(int total_checkins) {
        this.total_checkins = total_checkins;
    }

    public boolean is_checked_in() {
        return is_checked_in;
    }

    public void setIs_checked_in(boolean is_checked_in) {
        this.is_checked_in = is_checked_in;
    }

    public boolean isHas_delivery() {
        return has_delivery;
    }

    public void setHas_delivery(boolean has_delivery) {
        this.has_delivery = has_delivery;
    }

    public int getMax_capacity() {
        return max_capacity;
    }

    public void setMax_capacity(int max_capacity) {
        this.max_capacity = max_capacity;
    }

    public String getStore_status() {
        return store_status;
    }

    public void setStore_status(String store_status) {
        this.store_status = store_status;
    }

    public ArrayList<Thumbnail> getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(ArrayList<Thumbnail> thumbnails) {
        this.thumbnails = thumbnails;
    }

    public ArrayList<Reviews> getReviews() {
        return reviews;
    }

    public void setReviews(ArrayList<Reviews> reviews) {
        this.reviews = reviews;
    }

    public static class Location implements Parcelable {

        private int location_id;
        private Double latitude;
        private Double longitude;
        private String location_name;

        public Location() {
        }

        public Location(int location_id, Double latitude, Double longitude, String location_name) {
            this.location_id = location_id;
            this.latitude = latitude;
            this.longitude = longitude;
            this.location_name = location_name;
        }

        public int getLocation_id() {
            return location_id;
        }

        public void setLocation_id(int location_id) {
            this.location_id = location_id;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

        public String getLocation_name() {
            return location_name;
        }

        public void setLocation_name(String location_name) {
            this.location_name = location_name;
        }

        protected Location(Parcel in) {
            location_id = in.readInt();
            latitude = in.readByte() == 0x00 ? null : in.readDouble();
            longitude = in.readByte() == 0x00 ? null : in.readDouble();
            location_name = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(location_id);
            if (latitude == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeDouble(latitude);
            }
            if (longitude == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeDouble(longitude);
            }
            dest.writeString(location_name);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Location> CREATOR = new Parcelable.Creator<Location>() {
            @Override
            public Location createFromParcel(Parcel in) {
                return new Location(in);
            }

            @Override
            public Location[] newArray(int size) {
                return new Location[size];
            }
        };
    }

    public static class Category implements Parcelable {

        private int category_id;
        private String type;
        private String cuisine;
        private String more_info;
        private String added_info;

        public Category() {
        }

        public Category(int category_id, String type, String cuisine, String more_info, String added_info) {
            this.category_id = category_id;
            this.type = type;
            this.cuisine = cuisine;
            this.more_info = more_info;
            this.added_info = added_info;
        }

        public int getCategory_id() {
            return category_id;
        }

        public void setCategory_id(int category_id) {
            this.category_id = category_id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getCuisine() {
            return cuisine;
        }

        public void setCuisine(String cuisine) {
            this.cuisine = cuisine;
        }

        public String getMore_info() {
            return more_info;
        }

        public void setMore_info(String more_info) {
            this.more_info = more_info;
        }

        public String getAdded_info() {
            return added_info;
        }

        public void setAdded_info(String added_info) {
            this.added_info = added_info;
        }

        protected Category(Parcel in) {
            category_id = in.readInt();
            type = in.readString();
            cuisine = in.readString();
            more_info = in.readString();
            added_info = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(category_id);
            dest.writeString(type);
            dest.writeString(cuisine);
            dest.writeString(more_info);
            dest.writeString(added_info);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
            @Override
            public Category createFromParcel(Parcel in) {
                return new Category(in);
            }

            @Override
            public Category[] newArray(int size) {
                return new Category[size];
            }
        };
    }

    public static class Reviews implements Parcelable {
        private int id;
        private int user_id;
        private int link_id;
        private String module_type;
        private String content;
        private double ratings;
        private String created_at;
        private String updated_at;
        private String avatar;
        private String username;

        public Reviews() {
        }

        public Reviews(int id, int user_id, int link_id, String module_type, String content, double ratings, String created_at, String updated_at, String avatar, String username) {
            this.id = id;
            this.user_id = user_id;
            this.link_id = link_id;
            this.module_type = module_type;
            this.content = content;
            this.ratings = ratings;
            this.created_at = created_at;
            this.updated_at = updated_at;
            this.avatar = avatar;
            this.username = username;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getLink_id() {
            return link_id;
        }

        public void setLink_id(int link_id) {
            this.link_id = link_id;
        }

        public String getModule_type() {
            return module_type;
        }

        public void setModule_type(String module_type) {
            this.module_type = module_type;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public double getRatings() {
            return ratings;
        }

        public void setRatings(double ratings) {
            this.ratings = ratings;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        protected Reviews(Parcel in) {
            id = in.readInt();
            user_id = in.readInt();
            link_id = in.readInt();
            module_type = in.readString();
            content = in.readString();
            ratings = in.readDouble();
            created_at = in.readString();
            updated_at = in.readString();
            avatar = in.readString();
            username = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeInt(user_id);
            dest.writeInt(link_id);
            dest.writeString(module_type);
            dest.writeString(content);
            dest.writeDouble(ratings);
            dest.writeString(created_at);
            dest.writeString(updated_at);
            dest.writeString(avatar);
            dest.writeString(username);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Reviews> CREATOR = new Parcelable.Creator<Reviews>() {
            @Override
            public Reviews createFromParcel(Parcel in) {
                return new Reviews(in);
            }

            @Override
            public Reviews[] newArray(int size) {
                return new Reviews[size];
            }
        };
    }

    protected Restaurant(Parcel in) {
        id = in.readInt();
        user_id = in.readInt();
        location_id = in.readInt();
        resto_name = in.readString();
        owner_name = in.readString();
        email = in.readString();
        description = in.readString();
        category_id = in.readInt();
        address = in.readString();
        store_hours = in.readString();
        contact_no = in.readString();
        activation_status = in.readInt();
        created_at = in.readString();
        updated_at = in.readString();
        thumbnail = (Thumbnail) in.readValue(Thumbnail.class.getClassLoader());
        location = (Location) in.readValue(Location.class.getClassLoader());
        category = (Category) in.readValue(Category.class.getClassLoader());
        is_rated = in.readByte() != 0x00;
        ratings = in.readDouble();
        reviews_count = in.readInt();
        checkins_today = in.readInt();
        total_checkins = in.readInt();
        is_checked_in = in.readByte() != 0x00;
        has_delivery = in.readByte() != 0x00;
        max_capacity = in.readInt();
        store_status = in.readString();
        if (in.readByte() == 0x01) {
            thumbnails = new ArrayList<Thumbnail>();
            in.readList(thumbnails, Thumbnail.class.getClassLoader());
        } else {
            thumbnails = null;
        }
        if (in.readByte() == 0x01) {
            reviews = new ArrayList<Reviews>();
            in.readList(reviews, Reviews.class.getClassLoader());
        } else {
            reviews = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(user_id);
        dest.writeInt(location_id);
        dest.writeString(resto_name);
        dest.writeString(owner_name);
        dest.writeString(email);
        dest.writeString(description);
        dest.writeInt(category_id);
        dest.writeString(address);
        dest.writeString(store_hours);
        dest.writeString(contact_no);
        dest.writeInt(activation_status);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeValue(thumbnail);
        dest.writeValue(location);
        dest.writeValue(category);
        dest.writeByte((byte) (is_rated ? 0x01 : 0x00));
        dest.writeDouble(ratings);
        dest.writeInt(reviews_count);
        dest.writeInt(checkins_today);
        dest.writeInt(total_checkins);
        dest.writeByte((byte) (is_checked_in ? 0x01 : 0x00));
        dest.writeByte((byte) (has_delivery ? 0x01 : 0x00));
        dest.writeInt(max_capacity);
        dest.writeString(store_status);
        if (thumbnails == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(thumbnails);
        }
        if (reviews == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(reviews);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Restaurant> CREATOR = new Parcelable.Creator<Restaurant>() {
        @Override
        public Restaurant createFromParcel(Parcel in) {
            return new Restaurant(in);
        }

        @Override
        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };
}
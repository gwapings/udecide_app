package org.app.udecide.Model;


import android.os.Parcel;
import android.os.Parcelable;

public class Reservations implements Parcelable {
    private int id;
    private String confirmation_code;
    private int user_id;
    private int restaurant_id;
    private int num_seat;
    private String reservation_date;
    private int status;
    private String additional_request;
    private String remarks;
    private String created_at;
    private String updated_at;
    private String restaurant_name;
    private String reservation_date_formatted;
    private String created_at_formatted;
    private String label_status;

    public Reservations() {
    }

    public Reservations(int id, String confirmation_code, int user_id, int restaurant_id, int num_seat, String reservation_date, int status, String additional_request, String remarks, String created_at, String updated_at, String restaurant_name, String reservation_date_formatted, String created_at_formatted, String label_status) {
        this.id = id;
        this.confirmation_code = confirmation_code;
        this.user_id = user_id;
        this.restaurant_id = restaurant_id;
        this.num_seat = num_seat;
        this.reservation_date = reservation_date;
        this.status = status;
        this.additional_request = additional_request;
        this.remarks = remarks;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.restaurant_name = restaurant_name;
        this.reservation_date_formatted = reservation_date_formatted;
        this.created_at_formatted = created_at_formatted;
        this.label_status = label_status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConfirmation_code() {
        return confirmation_code;
    }

    public void setConfirmation_code(String confirmation_code) {
        this.confirmation_code = confirmation_code;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(int restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public int getNum_seat() {
        return num_seat;
    }

    public void setNum_seat(int num_seat) {
        this.num_seat = num_seat;
    }

    public String getReservation_date() {
        return reservation_date;
    }

    public void setReservation_date(String reservation_date) {
        this.reservation_date = reservation_date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAdditional_request() {
        return additional_request;
    }

    public void setAdditional_request(String additional_request) {
        this.additional_request = additional_request;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getReservation_date_formatted() {
        return reservation_date_formatted;
    }

    public void setReservation_date_formatted(String reservation_date_formatted) {
        this.reservation_date_formatted = reservation_date_formatted;
    }

    public String getCreated_at_formatted() {
        return created_at_formatted;
    }

    public void setCreated_at_formatted(String created_at_formatted) {
        this.created_at_formatted = created_at_formatted;
    }

    public String getLabel_status() {
        return label_status;
    }

    public void setLabel_status(String label_status) {
        this.label_status = label_status;
    }

    protected Reservations(Parcel in) {
        id = in.readInt();
        confirmation_code = in.readString();
        user_id = in.readInt();
        restaurant_id = in.readInt();
        num_seat = in.readInt();
        reservation_date = in.readString();
        status = in.readInt();
        additional_request = in.readString();
        remarks = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
        restaurant_name = in.readString();
        reservation_date_formatted = in.readString();
        created_at_formatted = in.readString();
        label_status = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(confirmation_code);
        dest.writeInt(user_id);
        dest.writeInt(restaurant_id);
        dest.writeInt(num_seat);
        dest.writeString(reservation_date);
        dest.writeInt(status);
        dest.writeString(additional_request);
        dest.writeString(remarks);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeString(restaurant_name);
        dest.writeString(reservation_date_formatted);
        dest.writeString(created_at_formatted);
        dest.writeString(label_status);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Reservations> CREATOR = new Parcelable.Creator<Reservations>() {
        @Override
        public Reservations createFromParcel(Parcel in) {
            return new Reservations(in);
        }

        @Override
        public Reservations[] newArray(int size) {
            return new Reservations[size];
        }
    };
}

package org.app.udecide.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Categories implements Parcelable {

    private int id;
    private String name;
    private String description;
    private String icon_path;
    private String created_at;
    private String updated_at;

    public Categories() {
    }

    public Categories(int id, String name, String description, String icon_path, String created_at, String updated_at) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.icon_path = icon_path;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon_path() {
        return icon_path;
    }

    public void setIcon_path(String icon_path) {
        this.icon_path = icon_path;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    protected Categories(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        icon_path = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(icon_path);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Categories> CREATOR = new Parcelable.Creator<Categories>() {
        @Override
        public Categories createFromParcel(Parcel in) {
            return new Categories(in);
        }

        @Override
        public Categories[] newArray(int size) {
            return new Categories[size];
        }
    };
}

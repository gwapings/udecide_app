package org.app.udecide.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class RestaurantMenu implements Parcelable {

    private int id;
    private int restaurant_id;
    private String food_name;
    private String description;
    private String menu_type;
    private double price;
    private double preparation_time;
    private String created_at;
    private String updated_at;
    private double ratings;
    private int reviews_count;
    private boolean is_rated;
    private ArrayList<Thumbnail> thumbnails = new ArrayList<Thumbnail>();
    private ArrayList<Reviews> reviews = new ArrayList<Reviews>();

    private boolean is_Checked = false;
    private int order_item_count = 1;

    public RestaurantMenu() {
    }

    public RestaurantMenu(int id, int restaurant_id, String food_name, String description, String menu_type, double price, double preparation_time, String created_at, String updated_at, double ratings, int reviews_count, boolean is_rated, ArrayList<Thumbnail> thumbnails, ArrayList<Reviews> reviews) {
        this.id = id;
        this.restaurant_id = restaurant_id;
        this.food_name = food_name;
        this.description = description;
        this.menu_type = menu_type;
        this.price = price;
        this.preparation_time = preparation_time;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.ratings = ratings;
        this.reviews_count = reviews_count;
        this.is_rated = is_rated;
        this.thumbnails = thumbnails;
        this.reviews = reviews;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(int restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getFood_name() {
        return food_name;
    }

    public void setFood_name(String food_name) {
        this.food_name = food_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMenu_type() {
        return menu_type;
    }

    public void setMenu_type(String menu_type) {
        this.menu_type = menu_type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPreparation_time() {
        return preparation_time;
    }

    public void setPreparation_time(double preparation_time) {
        this.preparation_time = preparation_time;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public double getRatings() {
        return ratings;
    }

    public void setRatings(double ratings) {
        this.ratings = ratings;
    }

    public int getReviews_count() {
        return reviews_count;
    }

    public void setReviews_count(int reviews_count) {
        this.reviews_count = reviews_count;
    }

    public boolean is_rated() {
        return is_rated;
    }

    public void setIs_rated(boolean is_rated) {
        this.is_rated = is_rated;
    }

    public ArrayList<Thumbnail> getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(ArrayList<Thumbnail> thumbnails) {
        this.thumbnails = thumbnails;
    }

    public ArrayList<Reviews> getReviews() {
        return reviews;
    }

    public void setReviews(ArrayList<Reviews> reviews) {
        this.reviews = reviews;
    }

    public int getOrder_item_count() {
        return order_item_count;
    }

    public void setOrder_item_count(int order_item_count) {
        this.order_item_count = order_item_count;
    }

    public boolean is_Checked() {
        return is_Checked;
    }

    public void setIs_Checked(boolean is_Checked) {
        this.is_Checked = is_Checked;
    }

    public static class Reviews {
        private int id;
        private int user_id;
        private int link_id;
        private String module_type;
        private String content;
        private double ratings;
        private String created_at;
        private String updated_at;
        private String avatar;
        private String username;

        public Reviews() {
        }

        public Reviews(int id, int user_id, int link_id, String module_type, String content, double ratings, String created_at, String updated_at, String avatar, String username) {
            this.id = id;
            this.user_id = user_id;
            this.link_id = link_id;
            this.module_type = module_type;
            this.content = content;
            this.ratings = ratings;
            this.created_at = created_at;
            this.updated_at = updated_at;
            this.avatar = avatar;
            this.username = username;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getLink_id() {
            return link_id;
        }

        public void setLink_id(int link_id) {
            this.link_id = link_id;
        }

        public String getModule_type() {
            return module_type;
        }

        public void setModule_type(String module_type) {
            this.module_type = module_type;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public double getRatings() {
            return ratings;
        }

        public void setRatings(double ratings) {
            this.ratings = ratings;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }


    protected RestaurantMenu(Parcel in) {
        id = in.readInt();
        restaurant_id = in.readInt();
        food_name = in.readString();
        description = in.readString();
        menu_type = in.readString();
        price = in.readDouble();
        preparation_time = in.readDouble();
        created_at = in.readString();
        updated_at = in.readString();
        ratings = in.readDouble();
        reviews_count = in.readInt();
        is_rated = in.readByte() != 0x00;
        if (in.readByte() == 0x01) {
            thumbnails = new ArrayList<Thumbnail>();
            in.readList(thumbnails, Thumbnail.class.getClassLoader());
        } else {
            thumbnails = null;
        }
        if (in.readByte() == 0x01) {
            reviews = new ArrayList<Reviews>();
            in.readList(reviews, Reviews.class.getClassLoader());
        } else {
            reviews = null;
        }
        is_Checked = in.readByte() != 0x00;
        order_item_count = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(restaurant_id);
        dest.writeString(food_name);
        dest.writeString(description);
        dest.writeString(menu_type);
        dest.writeDouble(price);
        dest.writeDouble(preparation_time);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeDouble(ratings);
        dest.writeInt(reviews_count);
        dest.writeByte((byte) (is_rated ? 0x01 : 0x00));
        if (thumbnails == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(thumbnails);
        }
        if (reviews == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(reviews);
        }
        dest.writeByte((byte) (is_Checked ? 0x01 : 0x00));
        dest.writeInt(order_item_count);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<RestaurantMenu> CREATOR = new Parcelable.Creator<RestaurantMenu>() {
        @Override
        public RestaurantMenu createFromParcel(Parcel in) {
            return new RestaurantMenu(in);
        }

        @Override
        public RestaurantMenu[] newArray(int size) {
            return new RestaurantMenu[size];
        }
    };
}

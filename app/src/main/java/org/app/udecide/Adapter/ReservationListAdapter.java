package org.app.udecide.Adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.app.udecide.Model.Reservations;
import org.app.udecide.R;
import org.app.udecide.helper.UDecideConstants;
import org.app.udecide.helper.UDecideHelper;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import vibetech.helper.httpclienthelper.HEADERS;
import vibetech.helper.httpclienthelper.HttpClientHelper;
import vibetech.helper.httpclienthelper.HttpClientInterface;
import vibetech.helper.httpclienthelper.ToastHandler;

public class ReservationListAdapter extends BaseAdapter {

    public interface onCancelReservation {
        void cancelReservation();
    }

    protected onCancelReservation mCancelListener;
    protected Context mContext;
    protected LayoutInflater mInflater;
    protected ArrayList<Reservations> reservations = new ArrayList<Reservations>();

    public ReservationListAdapter(Context ctx, onCancelReservation listener) {
        mCancelListener = listener;
        mContext = ctx;
        mInflater = LayoutInflater.from(ctx);
    }

    public void updateList(ArrayList<Reservations> data) {
        reservations = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return reservations.size();
    }

    @Override
    public Reservations getItem(int position) {
        return reservations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return reservations.get(position).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = mInflater.inflate(R.layout.reservations_item, null);
        }

        Reservations reservation = reservations.get(position);

        Button btnCancelReservation = (Button) view.findViewById(R.id.btn_cancel_reservation);
        btnCancelReservation.setTag(reservation);
        btnCancelReservation.setOnClickListener(onCancelClicked);

        TextView txtConfirmationCode = (TextView) view.findViewById(R.id.txt_res_confirmationcode);
        TextView txtRestoName = (TextView) view.findViewById(R.id.txt_res_resto_name);
        TextView txtResDate = (TextView) view.findViewById(R.id.txt_res_date);
        TextView txtNumSeat = (TextView) view.findViewById(R.id.txt_res_numseat);
        TextView txtCreatedAt = (TextView) view.findViewById(R.id.txt_res_date_created);
        TextView txtStatus = (TextView) view.findViewById(R.id.txt_res_status);
        TextView txtAdditionalRequest = (TextView) view.findViewById(R.id.txt_res_additionalrequest);
        TextView txtRemarks = (TextView) view.findViewById(R.id.txt_res_remarks);

        txtConfirmationCode.setText(reservation.getConfirmation_code());
        txtResDate.setText(reservation.getReservation_date_formatted());
        txtRestoName.setText(reservation.getRestaurant_name());
        txtNumSeat.setText(String.valueOf(reservation.getNum_seat()));
        txtCreatedAt.setText(reservation.getCreated_at_formatted());
        txtStatus.setText(reservation.getLabel_status());
        txtAdditionalRequest.setText(reservation.getAdditional_request());
        txtRemarks.setText((reservation.getRemarks() == null || reservation.getRemarks() == "null") ? "" : reservation.getRemarks());

        if (reservation.getStatus() == 2) {
            txtStatus.setTextColor(Color.RED);
            btnCancelReservation.setVisibility(View.GONE);
        } else if(reservation.getStatus() == 1) {
            txtStatus.setTextColor(Color.GREEN);
            btnCancelReservation.setVisibility(View.GONE);
        } else if(reservation.getStatus() == 0) {
            txtStatus.setTextColor(Color.BLUE);
            btnCancelReservation.setVisibility(View.VISIBLE);
        }

        return view;
    }

    private View.OnClickListener onCancelClicked = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final Reservations reservation = (Reservations) view.getTag();

            AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
            View cView = LayoutInflater.from(mContext).inflate(R.layout.cancel_reservation_modal, null);
            mBuilder.setView(cView);

            final EditText etxtRemarks = (EditText) cView.findViewById(R.id.cancel_remarks);

            mBuilder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        JSONObject objParams = new JSONObject();
                        objParams.put("reservation_id", reservation.getId());
                        objParams.put("remarks", etxtRemarks.getText().toString());

                        String cancel_url = UDecideConstants.BASE_URL + "api/restaurant/seat/reservation/cancel";
                        new HttpClientHelper().executeInBackground(mContext, cancel_url, objParams, null, true, true, new ArrayList<HEADERS>(), new HttpClientInterface() {
                            ProgressDialog loader;

                            @Override
                            public void onBeforeExecute(int i) {
                                loader = ProgressDialog.show(mContext, "", "loading....");
                            }

                            @Override
                            public void onHttpClientResponse(int i, String s) {
                                loader.setMessage("please wait....");
                                ToastHandler.ShowToast(mContext, "Reservation successfully cancelled.");
                            }

                            @Override
                            public void onHttpClientErrorException(int i, Exception e) {
                                loader.setMessage(e.getMessage());
                                loader.dismiss();
                            }

                            @Override
                            public void onAfterExecute(int i) {
                                mCancelListener.cancelReservation();
                                loader.dismiss();
                            }
                        }, 101010);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            mBuilder.setMessage("Are you sure do you want to cancel this reservation?");
            mBuilder.create().show();
        }
    };
}

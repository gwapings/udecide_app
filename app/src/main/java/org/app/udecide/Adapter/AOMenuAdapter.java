package org.app.udecide.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.app.udecide.CustomView.CustomNumberPicker;
import org.app.udecide.Model.RestaurantMenu;
import org.app.udecide.Model.Thumbnail;
import org.app.udecide.R;
import org.app.udecide.helper.AsyncBitmapLoader;
import org.app.udecide.helper.UDecideConstants;

import java.util.ArrayList;

import vibetech.helper.httpclienthelper.ToastHandler;

public class AOMenuAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<RestaurantMenu> food_menus = new ArrayList<RestaurantMenu>();
    private LayoutInflater mInflater;
    private OnAOMenuListener mMenuListener;

    public interface OnAOMenuListener {
        void onMenuImageClick(Thumbnail thumbnail, View view);

        void onMenuItemCheckSelect(ArrayList<RestaurantMenu> menus);

        void onMenuQuantityChanged(ArrayList<RestaurantMenu> menus);
    }

    public AOMenuAdapter(Context ctx, OnAOMenuListener listener) {
        mContext = ctx;
        mInflater = LayoutInflater.from(ctx);
        mMenuListener = listener;
    }

    public void updateMenus(ArrayList<RestaurantMenu> data) {
        food_menus = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return food_menus.size();
    }

    @Override
    public RestaurantMenu getItem(int position) {
        return food_menus.get(position);
    }

    @Override
    public long getItemId(int position) {
        return food_menus.get(position).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        RestaurantMenu menu = food_menus.get(position);
        view = mInflater.inflate(R.layout.ao_menu_item, null);
        renderData(view, menu, position);
        return view;
    }

    private void renderData(View view, final RestaurantMenu menu, int position) {
        TextView txtMenuName = (TextView) view.findViewById(R.id.ao_menu_name);
        TextView txtDescription = (TextView) view.findViewById(R.id.ao_menu_description);
        TextView txtPrice = (TextView) view.findViewById(R.id.ao_menu_price);
        LinearLayout mLinearThumbnailContainer = (LinearLayout) view.findViewById(R.id.ao_menu_thumbnails_container);
        CheckBox chkSelect = (CheckBox) view.findViewById(R.id.ao_chkbx);
        final CustomNumberPicker numPicker = (CustomNumberPicker) view.findViewById(R.id.ao_custom_number_picker);
        numPicker.setTag(menu);
        numPicker.setOnNumberChangeListener(numberPickerListener);

        txtMenuName.setText(menu.getFood_name());
        txtDescription.setText(menu.getDescription());
        txtPrice.setText("₱ " + String.valueOf(menu.getPrice()));

        for (final Thumbnail thumbnail : menu.getThumbnails()) {
            ImageView img_thumbnail = new ImageView(mContext);
            img_thumbnail.setTag(thumbnail);
            img_thumbnail.setPadding(8, 8, 8, 8);

            if (AsyncBitmapLoader.cancelPotentialWork(UDecideConstants.BASE_URL + thumbnail.getFile_path(), img_thumbnail)) {
                AsyncBitmapLoader task = new AsyncBitmapLoader(mContext, img_thumbnail, 350, 350);
                AsyncBitmapLoader.AsyncDrawable asyncDrawable =
                        new AsyncBitmapLoader.AsyncDrawable(mContext.getResources(), UDecideConstants.getDefaultImagePlaceholder(mContext), task);
                img_thumbnail.setImageDrawable(asyncDrawable);
                task.execute(UDecideConstants.BASE_URL + thumbnail.getFile_path());
            }

            mLinearThumbnailContainer.addView(img_thumbnail);
            img_thumbnail.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mMenuListener.onMenuImageClick(thumbnail, v);
                    return false;
                }
            });
        }

        if (menu.is_Checked()) {
            chkSelect.setChecked(true);
            numPicker.setVisibility(View.VISIBLE);
        } else {
            chkSelect.setChecked(false);
            numPicker.setVisibility(View.GONE);
        }

        food_menus.get(food_menus.indexOf(menu)).setOrder_item_count(menu.getOrder_item_count());
        numPicker.setCurrentQuantity(menu.getOrder_item_count());

        chkSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    food_menus.get(food_menus.indexOf(menu)).setIs_Checked(true);
                    numPicker.setVisibility(View.VISIBLE);
                } else {
                    food_menus.get(food_menus.indexOf(menu)).setIs_Checked(false);
                    numPicker.setVisibility(View.GONE);
                }
                mMenuListener.onMenuItemCheckSelect(food_menus);
            }
        });
    }

    private CustomNumberPicker.onNumberPickerChangeListener numberPickerListener = new CustomNumberPicker.onNumberPickerChangeListener() {
        @Override
        public void onItemMenuQuantityChange(int count, RestaurantMenu menu) {
            food_menus.get(food_menus.indexOf(menu)).setOrder_item_count(count);
            mMenuListener.onMenuQuantityChanged(food_menus);
        }
    };
}

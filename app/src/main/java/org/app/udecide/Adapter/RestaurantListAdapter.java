package org.app.udecide.Adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.test.RenamingDelegatingContext;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import org.app.udecide.Model.Restaurant;
import org.app.udecide.R;
import org.app.udecide.helper.AsyncBitmapLoader;
import org.app.udecide.helper.UDecideConstants;
import org.app.udecide.helper.UDecideHelper;

import java.util.ArrayList;

public class RestaurantListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Restaurant> mRestaurants = new ArrayList<Restaurant>();
    private LayoutInflater mInflater;
    private Location mLastLocation;

    public RestaurantListAdapter(Context ctx, ArrayList<Restaurant> arrRestaurants) {
        this.mContext = ctx;
        this.mRestaurants = arrRestaurants;
        this.mInflater = LayoutInflater.from(ctx);
    }

    public void updateRestaurantList(ArrayList<Restaurant> data) {
        this.mRestaurants = data;
        notifyDataSetChanged();
    }

    public void updateRestaurantDistance(Location location) {
        this.mLastLocation = location;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return this.mRestaurants.size();
    }

    @Override
    public Restaurant getItem(int i) {
        return mRestaurants.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mRestaurants.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = this.mInflater.inflate(R.layout.restaurant_list_item, viewGroup, false);

        Restaurant restaurant = this.mRestaurants.get(i);

        TextView txRestoTitle = (TextView) view.findViewById(R.id.resto_list_title);
        TextView txtRestoDescription = (TextView) view.findViewById(R.id.resto_list_description);
        TextView txtRestoAddress = (TextView) view.findViewById(R.id.resto_address);
        TextView txtRestoCategory = (TextView) view.findViewById(R.id.resto_category);
        TextView txtRestoDistance = (TextView) view.findViewById(R.id.resto_distance);
        TextView txtRestoStoreStatus = (TextView) view.findViewById(R.id.resto_store_status);
        ImageView imgIcon = (ImageView) view.findViewById(R.id.resto_list_icon);
        RatingBar ratings = (RatingBar) view.findViewById(R.id.rating_bar);

        txRestoTitle.setText(restaurant.getResto_name());
        txtRestoDescription.setText(restaurant.getDescription());
        txtRestoAddress.setText(restaurant.getAddress());
        txtRestoCategory.setText(restaurant.getCategory().getType());
        txtRestoStoreStatus.setText(restaurant.getStore_status());
        if (restaurant.getStore_status().equalsIgnoreCase("closed")) {
            txtRestoStoreStatus.setTextColor(Color.RED);
        } else {
            txtRestoStoreStatus.setTextColor(Color.BLUE);
        }

        if (AsyncBitmapLoader.cancelPotentialWork(UDecideConstants.BASE_URL + restaurant.getThumbnail().getFile_path(), imgIcon)) {
            AsyncBitmapLoader task = new AsyncBitmapLoader(mContext, imgIcon, 0, 0);
            AsyncBitmapLoader.AsyncDrawable asyncDrawable =
                    new AsyncBitmapLoader.AsyncDrawable(mContext.getResources(), UDecideConstants.getDefaultImagePlaceholder(mContext), task);
            imgIcon.setImageDrawable(asyncDrawable);
            task.execute(UDecideConstants.BASE_URL + restaurant.getThumbnail().getFile_path());
        }

        if (restaurant.getRatings() > 0) {
            ratings.setVisibility(View.VISIBLE);
            ratings.setRating((float) restaurant.getRatings());
            ratings.invalidate();
        } else {
            ratings.setVisibility(View.GONE);
        }

        if (mLastLocation != null) {
            txtRestoDistance.setVisibility(View.VISIBLE);
            double distance = new UDecideHelper().getDistanceBetweenLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), restaurant.getLocation().getLatitude(), restaurant.getLocation().getLongitude());
            txtRestoDistance.setText(String.format("%.2f", distance) + " km(s) away");
        } else {
            txtRestoDistance.setVisibility(View.GONE);
        }
        return view;
    }

}

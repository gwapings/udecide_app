package org.app.udecide.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.app.udecide.Model.AdvanceOrder;
import org.app.udecide.Model.Thumbnail;
import org.app.udecide.R;
import org.app.udecide.helper.AsyncBitmapLoader;
import org.app.udecide.helper.UDecideConstants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AOViewAdapter extends BaseAdapter {

    protected Context mContext;
    protected LayoutInflater mInflater;
    protected ArrayList<AdvanceOrder> advanceOrders = new ArrayList<AdvanceOrder>();

    public AOViewAdapter(Context ctx) {
        mContext = ctx;
        mInflater = LayoutInflater.from(ctx);
    }

    public void updateAdvanceOrders(ArrayList<AdvanceOrder> data) {
        advanceOrders = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return advanceOrders.size();
    }

    @Override
    public AdvanceOrder getItem(int i) {
        return advanceOrders.get(i);
    }

    @Override
    public long getItemId(int i) {
        return advanceOrders.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = mInflater.inflate(R.layout.ao_view_item, null);
        try {
            renderView(view, i);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return view;
    }

    private void renderView(View view, int position) throws ParseException {
        AdvanceOrder AO = advanceOrders.get(position);

        TextView txtRefCode = (TextView) view.findViewById(R.id.txt_ao_ref_code);
        TextView txtRestoName = (TextView) view.findViewById(R.id.txt_ao_resto_name);
        TextView txtTotalAmount = (TextView) view.findViewById(R.id.txt_ao_total_amount);
        TextView txtExpectedTime = (TextView) view.findViewById(R.id.txt_ao_expected_time);
        TextView txtStatus = (TextView) view.findViewById(R.id.txt_ao_status);
        TextView txtDateCreated = (TextView) view.findViewById(R.id.txt_ao_date);

        Button btnOrders = (Button) view.findViewById(R.id.btn_ao_orders);
        btnOrders.setTag(AO);
        btnOrders.setOnClickListener(orderClick);

        Button btnDeliveryDetails = (Button) view.findViewById(R.id.btn_ao_delivery);
        btnDeliveryDetails.setTag(AO);
        btnDeliveryDetails.setOnClickListener(deliveryDetailsClick);

        if (AO.getDelivery().getId() != 0) {
            btnDeliveryDetails.setVisibility(View.VISIBLE);
        } else {
            btnDeliveryDetails.setVisibility(View.GONE);
        }

        String status = "pending";
        if (AO.getStatus() == 1) {
            status = "Order Accepted";
        }

        SimpleDateFormat fmt2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date2 = fmt2.parse(AO.getExpectedTime());
        SimpleDateFormat fmtOut2 = new SimpleDateFormat("EEE, d MMM yyyy hh:mm aaa");
        txtExpectedTime.setText("[Exp. Time]: " + fmtOut2.format(date2));

        txtRefCode.setText(AO.getRef_code());
        txtRestoName.setText(AO.getRestaurant_name());
        txtTotalAmount.setText("₱ " + AO.getTotal_amount());
        txtStatus.setText(status);

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = fmt.parse(AO.getCreated_at());
        SimpleDateFormat fmtOut = new SimpleDateFormat("EEE, d MMM yyyy hh:mm aaa");
        txtDateCreated.setText(fmtOut.format(date));
    }

    private View.OnClickListener orderClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AdvanceOrder AO = (AdvanceOrder) view.getTag();
            ArrayList<AdvanceOrder.AdvanceOrderMenus> menus = AO.getOrders();

            Log.d("TEST", "menu count " + menus.size());

            final ScrollView mScrollView = new ScrollView(mContext);

            LinearLayout mLinParent = new LinearLayout(mContext);
            mLinParent.setOrientation(LinearLayout.VERTICAL);

            for (AdvanceOrder.AdvanceOrderMenus menu : menus) {
                View dlgView = LayoutInflater.from(mContext).inflate(R.layout.ao_orders_view, null);

                TextView txtAO_name = (TextView) dlgView.findViewById(R.id.ao_view_menu_name);
                TextView txtAO_desc = (TextView) dlgView.findViewById(R.id.ao_view_menu_description);
                TextView txtAO_price = (TextView) dlgView.findViewById(R.id.ao_view_menu_item_price);
                TextView txtAO_count = (TextView) dlgView.findViewById(R.id.ao_view_menu_item_count);
                TextView txtAO_total = (TextView) dlgView.findViewById(R.id.ao_view_menu_item_total_price);

                txtAO_name.setText(menu.getFood_name());
                txtAO_desc.setText(menu.getDescription());
                txtAO_price.setText("₱ " + menu.getPrice());
                txtAO_count.setText(menu.getItem_count() + "pc(s)");

                double totalAmount = menu.getPrice() * menu.getItem_count();
                txtAO_total.setText("₱ " + totalAmount);

                LinearLayout mLinearThumbnailContainer = (LinearLayout) dlgView.findViewById(R.id.ao_view_thumbnails_container);
                for (final Thumbnail thumbnail : menu.getThumbnails()) {
                    ImageView img_thumbnail = new ImageView(mContext);
                    img_thumbnail.setTag(thumbnail);
                    img_thumbnail.setPadding(8, 8, 8, 8);

                    if (AsyncBitmapLoader.cancelPotentialWork(UDecideConstants.BASE_URL + thumbnail.getFile_path(), img_thumbnail)) {
                        AsyncBitmapLoader task = new AsyncBitmapLoader(mContext, img_thumbnail, 350, 350);
                        AsyncBitmapLoader.AsyncDrawable asyncDrawable =
                                new AsyncBitmapLoader.AsyncDrawable(mContext.getResources(), UDecideConstants.getDefaultImagePlaceholder(mContext), task);
                        img_thumbnail.setImageDrawable(asyncDrawable);
                        task.execute(UDecideConstants.BASE_URL + thumbnail.getFile_path());
                    }

                    mLinearThumbnailContainer.addView(img_thumbnail);
                }

                mLinParent.addView(dlgView);
            }

            mScrollView.addView(mLinParent);

            AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
            mBuilder.setView(mScrollView);

            mBuilder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    mScrollView.removeAllViews();
                    dialogInterface.dismiss();
                }
            });

            mBuilder.create().show();
        }
    };

    private View.OnClickListener deliveryDetailsClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AdvanceOrder AO = (AdvanceOrder) view.getTag();

            View dlgView = LayoutInflater.from(mContext).inflate(R.layout.ao_delivery_details, null);
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
            mBuilder.setView(dlgView);

            TextView txtRefCode = (TextView) dlgView.findViewById(R.id.del_ref_code);
            txtRefCode.setText(AO.getDelivery().getRef_code());

            TextView txtstatus = (TextView) dlgView.findViewById(R.id.del_status);
            txtstatus.setText(getStatus(AO.getDelivery().getStatus()));

            TextView txtAddress = (TextView) dlgView.findViewById(R.id.del_address);
            txtAddress.setText(AO.getDelivery().getAddress());

            TextView txtDeliveryName = (TextView) dlgView.findViewById(R.id.del_delivery_name);
            txtDeliveryName.setText(AO.getDelivery().getDelivery_name());

            TextView txtRemarks = (TextView) dlgView.findViewById(R.id.del_remarks);
            txtRemarks.setText(AO.getDelivery().getRemarks());

            mBuilder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            mBuilder.create().show();
        }
    };

    private String getStatus(int stat) {
        String status = "n/a";

        if (stat == 0) {
            status = "Still On Queue";
        } else if (stat == 1) {
            status = "Food is on the way";
        } else if (stat == 2) {
            status = "Order is being prepared";
        } else if (stat == 3) {
            status = "Food delivered";
        }

        return status;
    }
}

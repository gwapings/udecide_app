package org.app.udecide.Parser;

import android.util.Log;

import org.app.udecide.Model.Restaurant;
import org.app.udecide.Model.RestaurantMenu;
import org.app.udecide.Model.Thumbnail;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RestaurantParser {

    public RestaurantParser() {
    }

    public Restaurant parseRestaurantSolo(String response) throws JSONException {
        Restaurant restaurant = new Restaurant();

        JSONObject objResto = new JSONObject(response);

        int id = objResto.getInt("id");
        int user_id = objResto.getInt("user_id");
        int location_id = objResto.getInt("location_id");
        String resto_name = objResto.getString("resto_name");
        String owner_name = objResto.getString("owner_name");
        String email = objResto.getString("email");
        String description = objResto.getString("description");
        int category_id = objResto.getInt("category_id");
        String address = objResto.getString("address");
        String store_hours = objResto.getString("store_hours");
        String contact_no = objResto.getString("contact_no");
        int activation_status = objResto.getInt("activation_status");
        String created_at = objResto.getString("created_at");
        String updated_at = objResto.getString("updated_at");
        boolean is_rated = objResto.getBoolean("is_rated");
        double ratings = objResto.getDouble("ratings");
        int reviews_count = objResto.getInt("reviews_count");
        int checkins_today = objResto.getInt("checkins_today");
        int total_checkins = objResto.getInt("total_checkins");
        boolean is_checked_in = objResto.getBoolean("is_checked_in");
        boolean has_delivery = objResto.getBoolean("has_delivery");
        int max_capacity = objResto.getInt("max_capacity");
        String store_status = objResto.getString("store_status");

        Thumbnail thumbnail = new Thumbnail();
        if (objResto.has("thumbnail") && !objResto.isNull("thumbnail")) {
            JSONObject objThumbnail = objResto.getJSONObject("thumbnail");
            int thumbnail_id = objThumbnail.getInt("id");
            String file_path = objThumbnail.getString("file_path");
            String module_type = objThumbnail.getString("module_type");
            int link_id = objThumbnail.getInt("link_id");
            int is_primary = objThumbnail.getInt("is_primary");
            int is_thumbnail = objThumbnail.getInt("is_thumbnail");
            String thumbnail_created_at = objThumbnail.getString("created_at");
            String thumbnail_updated_at = objThumbnail.getString("updated_at");
            thumbnail = new Thumbnail(thumbnail_id, file_path, module_type, link_id, is_primary, is_thumbnail, thumbnail_created_at, thumbnail_updated_at);
        }

        Restaurant.Location location = new Restaurant.Location();
        if (objResto.has("location") && !objResto.isNull("location")) {
            JSONObject objLocation = objResto.getJSONObject("location");
            int location_location_id = objLocation.getInt("location_id");
            Double latitude = objLocation.getDouble("latitude");
            Double longitude = objLocation.getDouble("longitude");
            String location_name = objLocation.getString("location_name");
            location = new Restaurant.Location(location_location_id, latitude, longitude, location_name);
        }

        Restaurant.Category category = new Restaurant.Category();
        if (objResto.has("category") && !objResto.isNull("category")) {
            JSONObject objCategory = objResto.getJSONObject("category");
            int category_category_id = objCategory.getInt("category_id");
            String cat_type = objCategory.getString("type");
            String cat_cuisine = objCategory.getString("cuisine");
            String cat_more_info = objCategory.getString("more_info");
            String cat_added_info = objCategory.getString("added_info");
            category = new Restaurant.Category(category_category_id, cat_type, cat_cuisine, cat_more_info, cat_added_info);
        }

        ArrayList<Thumbnail> arrThumbnails = new ArrayList<Thumbnail>();
        JSONArray objArrThumbnails = objResto.getJSONArray("thumbnails");
        for (int j = 0; j < objArrThumbnails.length(); j++) {
            JSONObject objSubThumbnail = objArrThumbnails.getJSONObject(j);

            int _thumbnail_id = objSubThumbnail.getInt("id");
            String _file_path = objSubThumbnail.getString("file_path");
            String _module_type = objSubThumbnail.getString("module_type");
            int _link_id = objSubThumbnail.getInt("link_id");
            int _is_primary = objSubThumbnail.getInt("is_primary");
            int _is_thumbnail = objSubThumbnail.getInt("is_thumbnail");
            String _thumbnail_created_at = objSubThumbnail.getString("created_at");
            String _thumbnail_updated_at = objSubThumbnail.getString("updated_at");
            Thumbnail sub_thumbnail = new Thumbnail(_thumbnail_id, _file_path, _module_type, _link_id, _is_primary, _is_thumbnail, _thumbnail_created_at, _thumbnail_updated_at);

            arrThumbnails.add(sub_thumbnail);
        }

        ArrayList<Restaurant.Reviews> arrReviews = new ArrayList<Restaurant.Reviews>();
        JSONArray objArrReviews = objResto.getJSONArray("reviews");
        for (int k = 0; k < objArrReviews.length(); k++) {
            JSONObject objReviews = objArrReviews.getJSONObject(k);
            int review_id = objReviews.getInt("id");
            int review_user_id = objReviews.getInt("user_id");
            int review_link_id = objReviews.getInt("link_id");
            String review_module_type = objReviews.getString("module_type");
            String review_content = objReviews.getString("content");
            double review_ratings = objReviews.getInt("ratings");
            String review_created_at = objReviews.getString("created_at");
            String review_updated_at = objReviews.getString("updated_at");
            String review_avatar = objReviews.getString("avatar");
            String review_username = objReviews.getString("username");
            Restaurant.Reviews reviews = new Restaurant.Reviews(review_id, review_user_id, review_link_id, review_module_type, review_content, review_ratings, review_created_at,
                    review_updated_at, review_avatar, review_username);

            arrReviews.add(reviews);
        }

        restaurant = new Restaurant(id, user_id, location_id, resto_name, owner_name, email, description, category_id, address, store_hours, contact_no, activation_status, created_at, updated_at, thumbnail, location, category, is_rated, ratings, reviews_count, checkins_today, total_checkins, is_checked_in, has_delivery, max_capacity, store_status, arrThumbnails, arrReviews);
        return restaurant;
    }

    public ArrayList<Restaurant> parseRestaurant(String response) {
        ArrayList<Restaurant> arrRestaurants = new ArrayList<Restaurant>();
        try {
            JSONArray arrResponse = new JSONArray(response);
            for (int i = 0; i < arrResponse.length(); i++) {
                JSONObject objResto = arrResponse.getJSONObject(i);

                int id = objResto.getInt("id");
                int user_id = objResto.getInt("user_id");
                int location_id = objResto.getInt("location_id");
                String resto_name = objResto.getString("resto_name");
                String owner_name = objResto.getString("owner_name");
                String email = objResto.getString("email");
                String description = objResto.getString("description");
                int category_id = objResto.getInt("category_id");
                String address = objResto.getString("address");
                String store_hours = objResto.getString("store_hours");
                String contact_no = objResto.getString("contact_no");
                int activation_status = objResto.getInt("activation_status");
                String created_at = objResto.getString("created_at");
                String updated_at = objResto.getString("updated_at");
                boolean is_rated = objResto.getBoolean("is_rated");
                double ratings = objResto.getDouble("ratings");
                int reviews_count = objResto.getInt("reviews_count");
                int checkins_today = objResto.getInt("checkins_today");
                int total_checkins = objResto.getInt("total_checkins");
                boolean is_checked_in = objResto.getBoolean("is_checked_in");
                boolean has_delivery = objResto.getBoolean("has_delivery");
                int max_capacity = objResto.getInt("max_capacity");
                String store_status = objResto.getString("store_status");

                Thumbnail thumbnail = new Thumbnail();
                if (objResto.has("thumbnail") && !objResto.isNull("thumbnail")) {
                    JSONObject objThumbnail = objResto.getJSONObject("thumbnail");
                    int thumbnail_id = objThumbnail.getInt("id");
                    String file_path = objThumbnail.getString("file_path");
                    String module_type = objThumbnail.getString("module_type");
                    int link_id = objThumbnail.getInt("link_id");
                    int is_primary = objThumbnail.getInt("is_primary");
                    int is_thumbnail = objThumbnail.getInt("is_thumbnail");
                    String thumbnail_created_at = objThumbnail.getString("created_at");
                    String thumbnail_updated_at = objThumbnail.getString("updated_at");
                    thumbnail = new Thumbnail(thumbnail_id, file_path, module_type, link_id, is_primary, is_thumbnail, thumbnail_created_at, thumbnail_updated_at);
                }

                Restaurant.Location location = new Restaurant.Location();
                if (objResto.has("location") && !objResto.isNull("location")) {
                    JSONObject objLocation = objResto.getJSONObject("location");
                    int location_location_id = objLocation.getInt("location_id");
                    Double latitude = objLocation.getDouble("latitude");
                    Double longitude = objLocation.getDouble("longitude");
                    String location_name = objLocation.getString("location_name");
                    location = new Restaurant.Location(location_location_id, latitude, longitude, location_name);
                }

                Restaurant.Category category = new Restaurant.Category();
                if (objResto.has("category") && !objResto.isNull("category")) {
                    JSONObject objCategory = objResto.getJSONObject("category");
                    int category_category_id = objCategory.getInt("category_id");
                    String cat_type = objCategory.getString("type");
                    String cat_cuisine = objCategory.getString("cuisine");
                    String cat_more_info = objCategory.getString("more_info");
                    String cat_added_info = objCategory.getString("added_info");
                    category = new Restaurant.Category(category_category_id, cat_type, cat_cuisine, cat_more_info, cat_added_info);
                }

                ArrayList<Thumbnail> arrThumbnails = new ArrayList<Thumbnail>();
                JSONArray objArrThumbnails = objResto.getJSONArray("thumbnails");
                for (int j = 0; j < objArrThumbnails.length(); j++) {
                    JSONObject objSubThumbnail = objArrThumbnails.getJSONObject(j);

                    int _thumbnail_id = objSubThumbnail.getInt("id");
                    String _file_path = objSubThumbnail.getString("file_path");
                    String _module_type = objSubThumbnail.getString("module_type");
                    int _link_id = objSubThumbnail.getInt("link_id");
                    int _is_primary = objSubThumbnail.getInt("is_primary");
                    int _is_thumbnail = objSubThumbnail.getInt("is_thumbnail");
                    String _thumbnail_created_at = objSubThumbnail.getString("created_at");
                    String _thumbnail_updated_at = objSubThumbnail.getString("updated_at");
                    Thumbnail sub_thumbnail = new Thumbnail(_thumbnail_id, _file_path, _module_type, _link_id, _is_primary, _is_thumbnail, _thumbnail_created_at, _thumbnail_updated_at);

                    arrThumbnails.add(sub_thumbnail);
                }

                ArrayList<Restaurant.Reviews> arrReviews = new ArrayList<Restaurant.Reviews>();
                JSONArray objArrReviews = objResto.getJSONArray("reviews");
                for (int k = 0; k < objArrReviews.length(); k++) {
                    JSONObject objReviews = objArrReviews.getJSONObject(k);
                    int review_id = objReviews.getInt("id");
                    int review_user_id = objReviews.getInt("user_id");
                    int review_link_id = objReviews.getInt("link_id");
                    String review_module_type = objReviews.getString("module_type");
                    String review_content = objReviews.getString("content");
                    double review_ratings = objReviews.getInt("ratings");
                    String review_created_at = objReviews.getString("created_at");
                    String review_updated_at = objReviews.getString("updated_at");
                    String review_avatar = objReviews.getString("avatar");
                    String review_username = objReviews.getString("username");
                    Restaurant.Reviews reviews = new Restaurant.Reviews(review_id, review_user_id, review_link_id, review_module_type, review_content, review_ratings, review_created_at,
                            review_updated_at, review_avatar, review_username);

                    arrReviews.add(reviews);
                }

                Restaurant restaurant = new Restaurant(id, user_id, location_id, resto_name, owner_name, email, description, category_id, address, store_hours, contact_no, activation_status, created_at, updated_at, thumbnail, location, category, is_rated, ratings, reviews_count, checkins_today, total_checkins, is_checked_in, has_delivery, max_capacity, store_status, arrThumbnails, arrReviews);
                arrRestaurants.add(restaurant);
            }
        } catch (JSONException e) {
            Log.d("TEST", "[ERROR][parseRestaurant] " + e.toString());
            e.printStackTrace();
            arrRestaurants = new ArrayList<Restaurant>();
        }
        return arrRestaurants;
    }

    public ArrayList<RestaurantMenu> parseRestaurantMenu(String response) {
        ArrayList<RestaurantMenu> arrRestaurantMenus = new ArrayList<RestaurantMenu>();

        try {
            JSONArray arrResponse = new JSONArray(response);
            for (int i = 0; i < arrResponse.length(); i++) {
                JSONObject objMenu = arrResponse.getJSONObject(i);

                int id = objMenu.getInt("id");
                int restaurant_id = objMenu.getInt("restaurant_id");
                String food_name = objMenu.getString("food_name");
                String description = objMenu.getString("description");
                String menu_type = objMenu.getString("menu_type");
                double price = objMenu.getDouble("price");
                double preparation_time = objMenu.getDouble("preparation_time");
                String created_at = objMenu.getString("created_at");
                String updated_at = objMenu.getString("updated_at");
                boolean is_rated = objMenu.getBoolean("is_rated");
                double ratings = objMenu.getDouble("ratings");
                int reviews_count = objMenu.getInt("reviews_count");

                ArrayList<Thumbnail> arrMenuThumbnails = new ArrayList<Thumbnail>();
                JSONArray arrThumbnails = objMenu.getJSONArray("thumbnails");
                for (int j = 0; j < arrThumbnails.length(); j++) {
                    JSONObject objThumbnails = arrThumbnails.getJSONObject(j);

                    int _thumbnail_id = objThumbnails.getInt("id");
                    String _file_path = objThumbnails.getString("file_path");
                    String _module_type = objThumbnails.getString("module_type");
                    int _link_id = objThumbnails.getInt("link_id");
                    int _is_primary = objThumbnails.getInt("is_primary");
                    int _is_thumbnail = objThumbnails.getInt("is_thumbnail");
                    String _thumbnail_created_at = objThumbnails.getString("created_at");
                    String _thumbnail_updated_at = objThumbnails.getString("updated_at");
                    Thumbnail thumbnail = new Thumbnail(_thumbnail_id, _file_path, _module_type, _link_id, _is_primary, _is_thumbnail, _thumbnail_created_at, _thumbnail_updated_at);

                    arrMenuThumbnails.add(thumbnail);
                }

                ArrayList<RestaurantMenu.Reviews> arrReviews = new ArrayList<RestaurantMenu.Reviews>();
                JSONArray objArrReviews = objMenu.getJSONArray("reviews");
                for (int k = 0; k < objArrReviews.length(); k++) {
                    JSONObject objReviews = objArrReviews.getJSONObject(k);
                    int review_id = objReviews.getInt("id");
                    int review_user_id = objReviews.getInt("user_id");
                    int review_link_id = objReviews.getInt("link_id");
                    String review_module_type = objReviews.getString("module_type");
                    String review_content = objReviews.getString("content");
                    double review_ratings = objReviews.getInt("ratings");
                    String review_created_at = objReviews.getString("created_at");
                    String review_updated_at = objReviews.getString("updated_at");
                    String review_avatar = objReviews.getString("avatar");
                    String review_username = objReviews.getString("username");
                    RestaurantMenu.Reviews reviews = new RestaurantMenu.Reviews(review_id, review_user_id, review_link_id, review_module_type, review_content, review_ratings, review_created_at,
                            review_updated_at, review_avatar, review_username);

                    arrReviews.add(reviews);
                }

                RestaurantMenu menu = new RestaurantMenu(id, restaurant_id, food_name, description, menu_type, price, preparation_time, created_at, updated_at, ratings, reviews_count, is_rated, arrMenuThumbnails, arrReviews);
                arrRestaurantMenus.add(menu);
            }
        } catch (JSONException e) {
            Log.d("TEST", "[ERROR] " + e.toString());
            e.printStackTrace();
            arrRestaurantMenus = new ArrayList<RestaurantMenu>();
        }
        return arrRestaurantMenus;
    }
}

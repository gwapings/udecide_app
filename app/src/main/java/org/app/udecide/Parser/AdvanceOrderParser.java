package org.app.udecide.Parser;

import android.util.Log;

import org.app.udecide.Model.AdvanceOrder;
import org.app.udecide.Model.Thumbnail;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdvanceOrderParser {

    public AdvanceOrderParser() {
    }

    public ArrayList<AdvanceOrder> parseAdvanceOrdersAll(String response) throws JSONException {
        ArrayList<AdvanceOrder> arrAdvanceOrders = new ArrayList<AdvanceOrder>();

        JSONArray arrResponse = new JSONArray(response);
        for (int i = 0; i < arrResponse.length(); i++) {
            JSONObject objAOrder = arrResponse.getJSONObject(i);

            int id = objAOrder.getInt("id");
            String ref_code = objAOrder.getString("ref_code");
            int user_id = objAOrder.getInt("user_id");
            int restaurant_id = objAOrder.getInt("restaurant_id");
            String restaurant_name = objAOrder.getString("resto_name");
            double total_amount = objAOrder.getDouble("total_amount");
            int status = objAOrder.getInt("status");
            String created_at = objAOrder.getString("created_at");
            String updated_at = objAOrder.getString("updated_at");
            String expected_time = objAOrder.getString("expected_time");

            ArrayList<AdvanceOrder.AdvanceOrderMenus> arrAdvanceOrder = new ArrayList<AdvanceOrder.AdvanceOrderMenus>();
            JSONArray arrOrders = objAOrder.getJSONArray("orders");
            for (int j = 0; j < arrOrders.length(); j++) {
                JSONObject objOrders = arrOrders.getJSONObject(j);

                int advance_order_id = objOrders.getInt("advance_order_id");
                int menu_id = objOrders.getInt("menu_id");
                int item_count = objOrders.getInt("item_count");
                String orders_created_at = objOrders.getString("created_at");
                String orders_updated_at = objOrders.getString("updated_at");
                int orders_id = objOrders.getInt("id");
                int orders_restaurant_id = objOrders.getInt("restaurant_id");
                String food_name = objOrders.getString("food_name");
                String description = objOrders.getString("description");
                String menu_type = objOrders.getString("menu_type");
                double price = objOrders.getDouble("price");
                double preparation_time = objOrders.getDouble("preparation_time");

                ArrayList<Thumbnail> arrMenuThumbnails = new ArrayList<Thumbnail>();
                JSONArray arrThumbnails = objOrders.getJSONArray("thumbnails");
                for (int k = 0; k < arrThumbnails.length(); k++) {
                    JSONObject objThumbnails = arrThumbnails.getJSONObject(k);

                    int _thumbnail_id = objThumbnails.getInt("id");
                    String _file_path = objThumbnails.getString("file_path");
                    String _module_type = objThumbnails.getString("module_type");
                    int _link_id = objThumbnails.getInt("link_id");
                    int _is_primary = objThumbnails.getInt("is_primary");
                    int _is_thumbnail = objThumbnails.getInt("is_thumbnail");
                    String _thumbnail_created_at = objThumbnails.getString("created_at");
                    String _thumbnail_updated_at = objThumbnails.getString("updated_at");
                    Thumbnail thumbnail = new Thumbnail(_thumbnail_id, _file_path, _module_type, _link_id, _is_primary, _is_thumbnail, _thumbnail_created_at, _thumbnail_updated_at);

                    arrMenuThumbnails.add(thumbnail);
                }

                AdvanceOrder.AdvanceOrderMenus advance_order = new AdvanceOrder.AdvanceOrderMenus(advance_order_id, menu_id, item_count, orders_created_at, orders_updated_at, orders_id, orders_restaurant_id, food_name, description, menu_type, price, preparation_time, arrMenuThumbnails);
                arrAdvanceOrder.add(advance_order);
            }

            AdvanceOrder.Delivery delivery = new AdvanceOrder.Delivery();
            if (objAOrder.has("delivery") && !objAOrder.isNull("delivery")) {
                JSONObject objDelivery = objAOrder.getJSONObject("delivery");

                int del_id = objDelivery.getInt("id");
                int del_ao_id = objDelivery.getInt("ao_id");
                String del_ref_code = objDelivery.getString("ref_code");
                int del_restaurant_id = objDelivery.getInt("restaurant_id");
                int del_foodie_id = objDelivery.getInt("foodie_id");
                String del_address = objDelivery.getString("address");
                String del_contact_number = objDelivery.getString("contact_number");
                int del_status = objDelivery.getInt("status");
                String del_remarks = (objDelivery.isNull("remarks")) ? "" : objDelivery.getString("remarks");
                String del_delivery_name = (objDelivery.isNull("delivery_name")) ? "" : objDelivery.getString("delivery_name");
                int del_updated_by = (objDelivery.isNull("updated_by")) ? -1 : objDelivery.getInt("updated_by");
                String del_created_at = objDelivery.getString("created_at");
                String del_updated_at = objDelivery.getString("updated_at");
                int del_user_id = objDelivery.getInt("user_id");
                String del_name = objDelivery.getString("name");
                String del_contact_no = objDelivery.getString("contact_no");
                String del_gender = objDelivery.getString("gender");
                String del_email = objDelivery.getString("email");
                String del_delivery_address = objDelivery.getString("delivery_address");

                delivery = new AdvanceOrder.Delivery(del_id, del_ao_id, del_ref_code, del_restaurant_id, del_foodie_id, del_address,
                        del_contact_number, del_status, del_remarks, del_delivery_name, del_updated_by, del_created_at, del_updated_at,
                        del_user_id, del_name, del_contact_no, del_gender, del_email, del_delivery_address);
            }

            AdvanceOrder order = new AdvanceOrder(id, ref_code, user_id, restaurant_id, restaurant_name, total_amount, status, created_at, updated_at, arrAdvanceOrder, delivery, expected_time);
            arrAdvanceOrders.add(order);
        }
        return arrAdvanceOrders;
    }

    public AdvanceOrder parseAdvanceOrder(String response) throws JSONException {
        JSONObject objAOrder = new JSONObject(response);

        int id = objAOrder.getInt("id");
        String ref_code = objAOrder.getString("ref_code");
        int user_id = objAOrder.getInt("user_id");
        int restaurant_id = objAOrder.getInt("restaurant_id");
        String restaurant_name = objAOrder.getString("resto_name");
        double total_amount = objAOrder.getDouble("total_amount");
        int status = objAOrder.getInt("status");
        String created_at = objAOrder.getString("created_at");
        String updated_at = objAOrder.getString("updated_at");
        String expected_time = objAOrder.getString("expected_time");

        ArrayList<AdvanceOrder.AdvanceOrderMenus> arrAdvanceOrder = new ArrayList<AdvanceOrder.AdvanceOrderMenus>();
        JSONArray arrOrders = objAOrder.getJSONArray("orders");
        for (int j = 0; j < arrOrders.length(); j++) {
            JSONObject objOrders = arrOrders.getJSONObject(j);

            int advance_order_id = objOrders.getInt("advance_order_id");
            int menu_id = objOrders.getInt("menu_id");
            int item_count = objOrders.getInt("item_count");
            String orders_created_at = objOrders.getString("created_at");
            String orders_updated_at = objOrders.getString("updated_at");
            int orders_id = objOrders.getInt("id");
            int orders_restaurant_id = objOrders.getInt("restaurant_id");
            String food_name = objOrders.getString("food_name");
            String description = objOrders.getString("description");
            String menu_type = objOrders.getString("menu_type");
            double price = objOrders.getDouble("price");
            double preparation_time = objOrders.getDouble("preparation_time");

            ArrayList<Thumbnail> arrMenuThumbnails = new ArrayList<Thumbnail>();
            JSONArray arrThumbnails = objOrders.getJSONArray("thumbnails");
            for (int k = 0; k < arrThumbnails.length(); k++) {
                JSONObject objThumbnails = arrThumbnails.getJSONObject(j);

                int _thumbnail_id = objThumbnails.getInt("id");
                String _file_path = objThumbnails.getString("file_path");
                String _module_type = objThumbnails.getString("module_type");
                int _link_id = objThumbnails.getInt("link_id");
                int _is_primary = objThumbnails.getInt("is_primary");
                int _is_thumbnail = objThumbnails.getInt("is_thumbnail");
                String _thumbnail_created_at = objThumbnails.getString("created_at");
                String _thumbnail_updated_at = objThumbnails.getString("updated_at");
                Thumbnail thumbnail = new Thumbnail(_thumbnail_id, _file_path, _module_type, _link_id, _is_primary, _is_thumbnail, _thumbnail_created_at, _thumbnail_updated_at);

                arrMenuThumbnails.add(thumbnail);
            }

            AdvanceOrder.AdvanceOrderMenus advance_order = new AdvanceOrder.AdvanceOrderMenus(advance_order_id, menu_id, item_count, orders_created_at, orders_updated_at, orders_id, orders_restaurant_id, food_name, description, menu_type, price, preparation_time, arrMenuThumbnails);
            arrAdvanceOrder.add(advance_order);
        }

        AdvanceOrder.Delivery delivery = new AdvanceOrder.Delivery();
        if (objAOrder.has("delivery") && !objAOrder.isNull("delivery")) {
            JSONObject objDelivery = objAOrder.getJSONObject("delivery");

            int del_id = objDelivery.getInt("id");
            int del_ao_id = objDelivery.getInt("ao_id");
            String del_ref_code = objDelivery.getString("ref_code");
            int del_restaurant_id = objDelivery.getInt("restaurant_id");
            int del_foodie_id = objDelivery.getInt("foodie_id");
            String del_address = objDelivery.getString("address");
            String del_contact_number = objDelivery.getString("contact_number");
            int del_status = objDelivery.getInt("status");
            String del_remarks = objDelivery.getString("remarks");
            String del_delivery_name = objDelivery.getString("delivery_name");
            int del_updated_by = objDelivery.getInt("updated_by");
            String del_created_at = objDelivery.getString("created_at");
            String del_updated_at = objDelivery.getString("updated_at");
            int del_user_id = objDelivery.getInt("user_id");
            String del_name = objDelivery.getString("name");
            String del_contact_no = objDelivery.getString("contact_no");
            String del_gender = objDelivery.getString("gender");
            String del_email = objDelivery.getString("email");
            String del_delivery_address = objDelivery.getString("delivery_address");

            delivery = new AdvanceOrder.Delivery(del_id, del_ao_id, del_ref_code, del_restaurant_id, del_foodie_id, del_address,
                    del_contact_number, del_status, del_remarks, del_delivery_name, del_updated_by, del_created_at, del_updated_at,
                    del_user_id, del_name, del_contact_no, del_gender, del_email, del_delivery_address);
        }

        return new AdvanceOrder(id, ref_code, user_id, restaurant_id, restaurant_name, total_amount, status, created_at, updated_at, arrAdvanceOrder, delivery, expected_time);
    }
}

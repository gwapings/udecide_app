package org.app.udecide.Parser;

import org.app.udecide.Model.Reservations;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReservationParser {
    public ReservationParser() {
    }

    public ArrayList<Reservations> parseReservations(String json) throws JSONException {
        ArrayList<Reservations> reservations = new ArrayList<Reservations>();

        JSONArray arrJson = new JSONArray(json);
        for (int i = 0; i < arrJson.length(); i++) {
            JSONObject objReservation = arrJson.getJSONObject(i);

            int id = objReservation.getInt("id");
            String confirmation_code = objReservation.getString("confirmation_code");
            int user_id = objReservation.getInt("user_id");
            int restaurant_id = objReservation.getInt("restaurant_id");
            int num_seat = objReservation.getInt("num_seat");
            String reservation_date = objReservation.getString("reservation_date");
            int status = objReservation.getInt("status");
            String additional_request = objReservation.getString("additional_request");
            String remarks = objReservation.getString("remarks");
            String created_at = objReservation.getString("created_at");
            String updated_at = objReservation.getString("updated_at");
            String restaurant_name = objReservation.getString("restaurant_name");
            String reservation_date_formatted = objReservation.getString("reservation_date_formatted");
            String created_at_formatted = objReservation.getString("created_at_formatted");
            String label_status = objReservation.getString("label_status");

            Reservations reservation = new Reservations(id, confirmation_code, user_id, restaurant_id, num_seat,
                    reservation_date, status, additional_request, remarks,
                    created_at, updated_at, restaurant_name, reservation_date_formatted,
                    created_at_formatted, label_status);
            reservations.add(reservation);
        }

        return reservations;
    }
}

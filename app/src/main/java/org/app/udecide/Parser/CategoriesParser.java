package org.app.udecide.Parser;

import org.app.udecide.Model.Categories;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoriesParser {

    public CategoriesParser() {
    }

    public ArrayList<Categories> parseCategories(String response) throws JSONException {
        ArrayList<Categories> arrCategories = new ArrayList<Categories>();

        JSONArray arrCat = new JSONArray(response);
        for (int i = 0; i < arrCat.length(); i++) {
            JSONObject objCat = arrCat.getJSONObject(i);

            int id = objCat.getInt("id");
            String name = objCat.getString("name");
            String description = objCat.getString("description");
            String icon_path = objCat.getString("icon_path");
            String created_at = objCat.getString("created_at");
            String updated_at = objCat.getString("updated_at");

            Categories category = new Categories(id, name, description, icon_path, created_at, updated_at);
            arrCategories.add(category);
        }

        return arrCategories;
    }
}

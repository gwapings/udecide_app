package org.app.udecide;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.app.udecide.Adapter.AOViewAdapter;
import org.app.udecide.Model.AdvanceOrder;
import org.app.udecide.Parser.AdvanceOrderParser;
import org.app.udecide.helper.UDecideConstants;
import org.app.udecide.helper.UDecideHelper;
import org.json.JSONException;

import java.util.ArrayList;

import vibetech.helper.httpclienthelper.HEADERS;
import vibetech.helper.httpclienthelper.HttpClientHelper;
import vibetech.helper.httpclienthelper.HttpClientInterface;
import vibetech.helper.httpclienthelper.ToastHandler;

public class FragmentAdvanceOrderView extends Fragment {
    private OnFragmentInteractionListener mListener;

    protected ListView mAdvanceOrderList;
    protected AOViewAdapter mAOAdapter;

    protected View mView;
    protected ArrayList<AdvanceOrder> mAdvanceOrders = new ArrayList<AdvanceOrder>();

    public FragmentAdvanceOrderView() {
    }

    public static FragmentAdvanceOrderView newInstance(String param1, String param2) {
        FragmentAdvanceOrderView fragment = new FragmentAdvanceOrderView();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //TODO: if has arguments
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_advance_order_view, container, false);
        initViews();
        return mView;
    }

    private void initViews() {
        mAOAdapter = new AOViewAdapter(getContext());
        mAdvanceOrderList = (ListView) mView.findViewById(R.id.ao_list_view);
        mAdvanceOrderList.setAdapter(mAOAdapter);

        getAdvanceOrders();
    }

    private void getAdvanceOrders() {
        String user_id = new UDecideHelper().getUserId(getContext());
        String url = UDecideConstants.BASE_URL + "api/restaurant/advance_order/all/" + user_id;
        Log.d("TEST", "[getAdvanceOrders][URL]|===>" + url);
        new HttpClientHelper().executeInBackground(getContext(), url, null, null, false, false, new ArrayList<HEADERS>(), new HttpClientInterface() {
            ProgressDialog loader;

            @Override
            public void onBeforeExecute(int i) {
                loader = ProgressDialog.show(getActivity(), "", "loading....");
            }

            @Override
            public void onHttpClientResponse(int i, String s) {
                Log.d("TEST", "[getAdvanceOrders][onHttpClientResponse][response]|===>" + s);
                try {
                    loader.setMessage("please wait....");
                    mAdvanceOrders = new AdvanceOrderParser().parseAdvanceOrdersAll(s);
                    Log.d("TEST", "" + mAdvanceOrders.size());
                    mAOAdapter.updateAdvanceOrders(mAdvanceOrders);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("TEST", "" + e.toString());
                    loader.setMessage(e.getMessage());
                }
            }

            @Override
            public void onHttpClientErrorException(int i, Exception e) {
                Log.d("TEST", "[getAdvanceOrders][onHttpClientErrorException][error]|===>" + e.toString());
                loader.setMessage(e.getMessage());
                loader.dismiss();
            }

            @Override
            public void onAfterExecute(int i) {
                loader.dismiss();
            }
        }, 1000);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}

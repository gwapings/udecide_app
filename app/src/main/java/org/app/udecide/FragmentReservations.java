package org.app.udecide;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.app.udecide.Adapter.ReservationListAdapter;
import org.app.udecide.Model.Reservations;
import org.app.udecide.Parser.ReservationParser;
import org.app.udecide.helper.UDecideConstants;
import org.app.udecide.helper.UDecideHelper;
import org.json.JSONException;

import java.util.ArrayList;

import vibetech.helper.httpclienthelper.HEADERS;
import vibetech.helper.httpclienthelper.HttpClientHelper;
import vibetech.helper.httpclienthelper.HttpClientInterface;

public class FragmentReservations extends Fragment implements ReservationListAdapter.onCancelReservation {
    private OnFragmentInteractionListener mListener;

    protected View mView;

    protected ListView mReservationList;
    protected ReservationListAdapter mListAdapter;

    public FragmentReservations() {
    }

    public static FragmentReservations newInstance(String param1, String param2) {
        FragmentReservations fragment = new FragmentReservations();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_reservations, container, false);
            initViews();
        }
        return mView;
    }

    private void initViews() {
        mReservationList = (ListView) mView.findViewById(R.id.reservation_list);
        mListAdapter = new ReservationListAdapter(getContext(), this);
        mReservationList.setAdapter(mListAdapter);

        getReservations();
    }

    private void getReservations() {
        String reservation_url = UDecideConstants.BASE_URL + "api/restaurant/reservations/" + new UDecideHelper().getUserId(getContext());
        new HttpClientHelper().executeInBackground(getContext(), reservation_url, null, null, false, false, new ArrayList<HEADERS>(), new HttpClientInterface() {
            @Override
            public void onBeforeExecute(int i) {

            }

            @Override
            public void onHttpClientResponse(int i, String s) {
                try {
                    ArrayList<Reservations> reservations = new ReservationParser().parseReservations(s);
                    mListAdapter.updateList(reservations);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onHttpClientErrorException(int i, Exception e) {

            }

            @Override
            public void onAfterExecute(int i) {

            }
        }, 1001);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void cancelReservation() {
        getReservations();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}

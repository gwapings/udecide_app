package org.app.udecide.CustomView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by ullen on 1/20/17.
 */

public class CustomImageDivider extends ImageView {

    public CustomImageDivider(Context context) {
        super(context);
        LinearLayout.LayoutParams div_params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        div_params.setMargins(0, 10, 0, 10);
        Bitmap img = BitmapFactory.decodeResource(context.getResources(),
                android.R.drawable.divider_horizontal_textfield);
        setImageBitmap(img);
        setPadding(10, 0, 10, 0);
        setScaleType(ScaleType.FIT_XY);
        setLayoutParams(div_params);
    }

}

package org.app.udecide.CustomView;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.app.udecide.Model.RestaurantMenu;
import org.app.udecide.R;

/**
 * Created by ullen on 1/24/17.
 */

public class CustomNumberPicker extends LinearLayout {

    protected Context mContext;
    protected View mView;
    protected Button mBtnPlus;
    protected Button mBtnMinus;
    protected EditText mEdtxtQuantity;
    protected int mCurrentQuantity = 1;
    protected onNumberPickerChangeListener mListener;

    public interface onNumberPickerChangeListener {
        void onItemMenuQuantityChange(int count, RestaurantMenu menu);
    }

    public CustomNumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        setupViews(context);
    }

    public CustomNumberPicker(Context context) {
        super(context);
        mContext = context;
        setupViews(context);
    }

    protected void setupViews(Context context) {
        mView = LayoutInflater.from(context).inflate(R.layout.custom_number_picker, this, true);

        mEdtxtQuantity = (EditText) mView.findViewById(R.id.custom_quantity);
        mBtnPlus = (Button) mView.findViewById(R.id.custom_btn_plus);
        mBtnMinus = (Button) mView.findViewById(R.id.custom_btn_minus);

        mBtnPlus.setOnClickListener(onPlusListener);
        mBtnMinus.setOnClickListener(onMinusListener);

        mEdtxtQuantity.setText(String.valueOf(mCurrentQuantity));
        mEdtxtQuantity.addTextChangedListener(onChangeListener);
    }

    protected TextWatcher onChangeListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence kar, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence kar, int i, int i1, int i2) {
            if (Integer.parseInt(kar.toString()) == 0) {
                mBtnMinus.setEnabled(false);
            } else {
                mBtnMinus.setEnabled(true);
                if (mListener != null) {
                    mListener.onItemMenuQuantityChange(Integer.parseInt(kar.toString()), (RestaurantMenu) getTag());
                }
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    protected OnClickListener onPlusListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            mCurrentQuantity += 1;
            mEdtxtQuantity.setText(String.valueOf(mCurrentQuantity));
        }
    };

    protected OnClickListener onMinusListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            mCurrentQuantity -= 1;
            mEdtxtQuantity.setText(String.valueOf(mCurrentQuantity));
        }
    };

    public void setOnNumberChangeListener(onNumberPickerChangeListener listener) {
        mListener = listener;
    }

    public int getQuantity() {
        return mCurrentQuantity;
    }

    public void setCurrentQuantity(int count) {
        mCurrentQuantity = count;
        mEdtxtQuantity.setText(String.valueOf(count));
    }
}

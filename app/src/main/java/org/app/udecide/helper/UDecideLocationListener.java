package org.app.udecide.helper;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

public class UDecideLocationListener implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    protected Location mLastLocation;
    protected LocationRequest mLocationRequest;
    protected GoogleApiClient mGoogleApiClient;
    protected Context mContext;

    public interface UDecideLocationListenerInterface {
        public void onLocationChanged(Location location);

        public void onConnected();

        public void onDisconnected();
    }

    protected UDecideLocationListenerInterface location_interface;

    public UDecideLocationListener(Context ctx, UDecideLocationListenerInterface listener) {
        this.location_interface = listener;
        this.mContext = ctx;
        buildGoogleApiClient();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        //Toast.makeText(mContext, "onConnected", Toast.LENGTH_SHORT).show();
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if (mLastLocation != null) {
            location_interface.onLocationChanged(mLastLocation);
        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(20000); //3 seconds
        mLocationRequest.setFastestInterval(20000); //3 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        location_interface.onConnected();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Toast.makeText(mContext, "Connection Suspended...", Toast.LENGTH_SHORT).show();
        location_interface.onDisconnected();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Toast.makeText(mContext, "Connection Failed...", Toast.LENGTH_SHORT).show();
        location_interface.onDisconnected();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mLastLocation != null) {
            location_interface.onLocationChanged(mLastLocation);
        }

        //stop location updates
//        if (mGoogleApiClient != null) {
//            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
//        }
    }
}

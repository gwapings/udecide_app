package org.app.udecide.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.app.udecide.R;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

public class MapRouteDirections implements GMapV2Direction.DirectionReceivedListener {

    protected GoogleMap mMap;
    protected Context mContext;

    public MapRouteDirections(Context ctx, GoogleMap map) {
        this.mMap = map;
        this.mContext = ctx;
    }

    public void findDirection(LatLng startLatlng, LatLng destinationLatlng,
                              String startTitle, String destinationTitle, String startSnippet,
                              String destinationSnippet) {
        mMap.clear();

        MarkerOptions mDestination = new MarkerOptions()
                .position(destinationLatlng).title(destinationTitle)
                .snippet(destinationSnippet)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

        MarkerOptions mStart = new MarkerOptions().position(startLatlng)
                .title(startTitle).snippet(startSnippet)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        Marker destMarker = mMap.addMarker(mDestination);
        destMarker.showInfoWindow();
        mMap.addMarker(mStart);

        new GetRotueListTask(mContext, startLatlng, destinationLatlng,
                GMapV2Direction.MODE_DRIVING, Color.RED, this).execute();
    }

    @Override
    public void OnDirectionListReceived(final List<LatLng> mPointList, int lineColor) {
        if (mPointList != null) {
            PolylineOptions rectLine = new PolylineOptions().width(5).color(
                    lineColor);
            for (int i = 0; i < mPointList.size(); i++) {
                Log.d("TEST",
                        mPointList.get(i).latitude + " - "
                                + mPointList.get(i).longitude);
                rectLine.add(mPointList.get(i));
            }
            mMap.addPolyline(rectLine);

            CameraPosition mCPFrom = new CameraPosition.Builder()
                    .target(mPointList.get(0)).zoom(12.5f).bearing(0).tilt(50)
                    .build();
            final CameraPosition mCPTo = new CameraPosition.Builder()
                    .target(mPointList.get(mPointList.size() - 1)).zoom(12.5f)
                    .bearing(0).tilt(75).build();

            changeCamera(CameraUpdateFactory.newCameraPosition(mCPFrom),
                    new GoogleMap.CancelableCallback() {
                        @Override
                        public void onFinish() {
                            changeCamera(CameraUpdateFactory
                                            .newCameraPosition(mCPTo),
                                    new GoogleMap.CancelableCallback() {

                                        @Override
                                        public void onFinish() {

                                            LatLngBounds bounds = new LatLngBounds.Builder()
                                                    .include(mPointList.get(0))
                                                    .include(
                                                            mPointList
                                                                    .get(mPointList
                                                                            .size() - 1))
                                                    .build();
                                            changeCamera(
                                                    CameraUpdateFactory
                                                            .newLatLngBounds(
                                                                    bounds, 50),
                                                    null, false);
                                        }

                                        @Override
                                        public void onCancel() {
                                        }
                                    }, false);
                        }

                        @Override
                        public void onCancel() {
                        }
                    }, true);
        }
    }

    /**
     * Change the camera position by moving or animating the camera depending on
     * input parameter.
     */
    private void changeCamera(CameraUpdate update, GoogleMap.CancelableCallback callback,
                              boolean instant) {

        if (instant) {
            mMap.animateCamera(update, 1, callback);
        } else {
            mMap.animateCamera(update, 4000, callback);
        }
    }
}

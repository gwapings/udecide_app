package org.app.udecide.helper;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import org.app.udecide.Model.Restaurant;
import org.app.udecide.Parser.RestaurantParser;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import vibetech.helper.httpclienthelper.HEADERS;
import vibetech.helper.httpclienthelper.HttpClientHelper;
import vibetech.helper.httpclienthelper.HttpClientInterface;

public class UDecideBackgroundService extends Service implements UDecideLocationListener.UDecideLocationListenerInterface, HttpClientInterface {

    public static final String UDecideBroadcaster = "org.app.udecide.helper";

    protected Location mCurrentLocation;
    protected HttpClientHelper mHTTPHelper;
    protected UDecideHelper mUDecideHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onCreate]");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onStartCommand]");
        mHTTPHelper = new HttpClientHelper();
        mUDecideHelper = new UDecideHelper();
        new UDecideLocationListener(getApplicationContext(), this);

        threadChecker();

        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onBind]");
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onDestroy]");
    }   

    @Override
    public void onLocationChanged(Location location) {
        Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onLocationChanged]");
        try {
            mCurrentLocation = location;

            Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onLocationChanged][latitude]|-> " + mCurrentLocation.getLatitude());
            Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onLocationChanged][longitude]|-> " + mCurrentLocation.getLongitude());

            checkAllRestaurant();

            Intent intent = new Intent(UDecideBroadcaster);
            intent.putExtra("location_listener", location);
            sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected() {
        Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onConnected]");
    }

    @Override
    public void onDisconnected() {
        Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onDisconnected]");
    }

    @Override
    public void onBeforeExecute(int i) {
        Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onBeforeExecute] |-> " + i);
    }

    @Override
    public void onHttpClientResponse(int i, String s) {
        Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onHttpClientResponse] |-> " + s);
        ArrayList<Restaurant> restaurants = new RestaurantParser().parseRestaurant(s);
        checkoutRestaurants(restaurants);
    }

    @Override
    public void onHttpClientErrorException(int i, Exception e) {
        Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onHttpClientErrorException] |-> " + e.getMessage());
        stopSelf();
    }

    @Override
    public void onAfterExecute(int i) {
        Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onAfterExecute] |-> " + i);
    }

    protected void checkAllRestaurant() {
        HEADERS headers = new HEADERS();
        headers.addHeaders("UDecide_USERID", new UDecideHelper().getUserId(getApplicationContext()));
        String restaurantListURL = UDecideConstants.BASE_URL + "api/restaurant/list";
        mHTTPHelper.executeInBackground(getApplicationContext(), restaurantListURL, null, null, false, false, headers.getArrHeaders(), this, 1000);
    }

    protected void checkoutRestaurants(ArrayList<Restaurant> restaurants) {
        for (final Restaurant restaurant : restaurants) {
            double distance = mUDecideHelper.getDistanceBetweenLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), restaurant.getLocation().getLatitude(), restaurant.getLocation().getLongitude());
            if (restaurant != null && mCurrentLocation != null) {
//                Log.d("RESTO_SERVICE", "[UDecideBackgroundService][checkoutRestaurants][user_id] |-> " + mUDecideHelper.getUserId(getApplicationContext()));
//                Log.d("RESTO_SERVICE", "[UDecideBackgroundService][checkoutRestaurants][restaurant_id] |-> " + restaurant.getId());
//                Log.d("RESTO_SERVICE", "[UDecideBackgroundService][checkoutRestaurants][restaurant] |-> " + restaurant.getResto_name());
//                Log.d("RESTO_SERVICE", "[UDecideBackgroundService][checkoutRestaurants][current_latitude] |-> " + mCurrentLocation.getLatitude());
//                Log.d("RESTO_SERVICE", "[UDecideBackgroundService][checkoutRestaurants][current_longitude] |-> " + mCurrentLocation.getLongitude());
//                Log.d("RESTO_SERVICE", "[UDecideBackgroundService][checkoutRestaurants][distance] |-> " + distance);
                if (distance > UDecideConstants.CHECKOUT_DISTANCE) {
                    Log.d("RESTO_SERVICE", "[UDecideBackgroundService][checkoutRestaurants][OUT-OF-THE-RANGE]");
                    if (restaurant.is_checked_in()) {
                        // checkout user when not in range
                        mUDecideHelper.checkoutRestaurant(getApplicationContext(), restaurant.getId(), new UDecideHelper.onCheckoutRestaurantListener() {
                            @Override
                            public void onCheckoutRestaurantComplete() {
                                Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onCheckoutRestaurantComplete]");
                            }
                        });
                    }
                }
            }
            Log.d("RESTO_SERVICE", "--------------------------------------------------------------------------");
        }
    }

    protected void threadChecker() {
        final Handler handler = new Handler();
        TimerTask timertask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
//                        Log.d("RESTO_SERVICE", "[UDecideBackgroundService][onStartCommand][Timer Task Running...]");
                    }
                });
            }
        };
        Timer timer = new Timer();
        timer.schedule(timertask, 0, 5000);
    }
}

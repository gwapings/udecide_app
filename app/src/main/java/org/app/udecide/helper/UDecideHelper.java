package org.app.udecide.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.app.udecide.R;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import vibetech.helper.httpclienthelper.HEADERS;
import vibetech.helper.httpclienthelper.HttpClientHelper;
import vibetech.helper.httpclienthelper.HttpClientInterface;
import vibetech.helper.httpclienthelper.ToastHandler;

public class UDecideHelper {

    public UDecideHelper() {
    }

    public boolean isLoggedIn(Activity act) {
        SharedPreferences prefs = act.getSharedPreferences(
                "login_preferences", Context.MODE_PRIVATE);
        return prefs.getBoolean("is_logged_in", false);
    }

    public boolean isGuest(Activity act) {
        SharedPreferences prefs = act.getSharedPreferences(
                "login_preferences", Context.MODE_PRIVATE);
        return prefs.getBoolean("is_guest", false);
    }

    public void logout(Activity act) {
        SharedPreferences prefs = act.getSharedPreferences(
                "login_preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("is_logged_in");
        editor.remove("is_guest");
        editor.remove("response");
        editor.commit();
    }

    public String getUsername(Activity act) {
        String username = "n/a";
        SharedPreferences prefs = act.getSharedPreferences(
                "login_preferences", Context.MODE_PRIVATE);
        String response = prefs.getString("response", null);
        if (response != null) {
            try {
                JSONObject obj = new JSONObject(response);
                JSONObject data = obj.getJSONObject("data");
                username = data.getString("username");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return username;
    }

    public String getUserId(Context ctx) {
        String user_id = null;
        SharedPreferences prefs = ctx.getSharedPreferences(
                "login_preferences", Context.MODE_PRIVATE);
        String response = prefs.getString("response", null);
        if (response != null) {
            try {
                JSONObject obj = new JSONObject(response);
                JSONObject data = obj.getJSONObject("data");
                user_id = data.getString("id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return user_id;
    }

    public String getUserAvatar(Context ctx) {
        String avatar = null;
        SharedPreferences prefs = ctx.getSharedPreferences(
                "login_preferences", Context.MODE_PRIVATE);
        String response = prefs.getString("response", null);
        if (response != null) {
            try {
                JSONObject obj = new JSONObject(response);
                JSONObject data = obj.getJSONObject("data");
                avatar = data.getString("avatar");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return avatar;
    }

    public Bitmap getbmpfromURL(Context mContext, String surl) {
        try {
            URL url = new URL(surl);
            HttpURLConnection urlcon = (HttpURLConnection) url.openConnection();
            urlcon.setDoInput(true);
            urlcon.connect();
            InputStream in = urlcon.getInputStream();
            Bitmap mIcon = BitmapFactory.decodeStream(in);
            return mIcon;
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
            Bitmap mIcon = BitmapFactory.decodeResource(mContext.getResources(),
                    R.mipmap.app_icon);
            return mIcon;
        }
    }

    public double getDistanceBetweenLocation(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    protected double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    protected double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    public interface onCheckoutRestaurantListener {
        public void onCheckoutRestaurantComplete();
    }

    public void checkoutRestaurant(final Context ctx, int restaurant_id, final onCheckoutRestaurantListener listener) {
        try {
            JSONObject objReg = new JSONObject();
            objReg.put("user_id", new UDecideHelper().getUserId(ctx));
            objReg.put("restaurant_id", restaurant_id);

            String checkoutUrl = UDecideConstants.BASE_URL + "api/restaurant/checkout";
            new HttpClientHelper().executeInBackground(ctx, checkoutUrl, objReg, null, true, true, new ArrayList<HEADERS>(), new HttpClientInterface() {

                @Override
                public void onBeforeExecute(int i) {
                    Log.d("TEST", "(onBeforeExecute) i - value -> " + i);
                }

                @Override
                public void onHttpClientResponse(int i, String s) {
                    Log.d("TEST", "(onHttpClientResponse) i - value -> " + i);
                    Log.d("TEST", "(onHttpClientResponse) String s -> " + s);
                }

                @Override
                public void onHttpClientErrorException(int i, Exception e) {
                    Log.d("TEST", "(onHttpClientErrorException) i - value -> " + i);
                    Log.d("TEST", "(onHttpClientErrorException) i - msg -> " + e.toString());
                }

                @Override
                public void onAfterExecute(int i) {
                    Log.d("TEST", "(onAfterExecute) i - value -> " + i);
                    listener.onCheckoutRestaurantComplete();
                }
            }, 3254);
        } catch (JSONException e) {
            e.printStackTrace();
            ToastHandler.ShowToast(ctx, "[ERROR] |-> " + e.getMessage());
        }
    }
}

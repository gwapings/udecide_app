package org.app.udecide.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class GetRotueListTask extends AsyncTask<Void, Void, Void> {
    private final Context mContext;
    GMapV2Direction mGMDirection = new GMapV2Direction();
    LatLng fromPosition;
    LatLng toPosition;
    List<LatLng> mPointList;
    private int mDirectionMode, mLineColor;

    GMapV2Direction.DirectionReceivedListener mListener;

    public GetRotueListTask(Context context, LatLng fromPosition,
                            LatLng toPosition, int mDirectionMode, int color,
                            GMapV2Direction.DirectionReceivedListener mListener) {
        this.mContext = context;
        this.fromPosition = fromPosition;
        this.toPosition = toPosition;
        this.mDirectionMode = mDirectionMode;
        this.mListener = mListener;
        this.mLineColor = color;
    }

    @Override
    protected Void doInBackground(Void... params) {
        mGMDirection.setParams(fromPosition, toPosition, mDirectionMode);
        mPointList = mGMDirection.getPointList(this.mContext);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (mPointList != null) {
            mListener.OnDirectionListReceived(mPointList, mLineColor);
        } else {
            Toast.makeText(this.mContext, "Error downloading direction!",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPreExecute() {
        ConnectivityManager conMgr = (ConnectivityManager) mContext
                .getApplicationContext().getSystemService(
                        Context.CONNECTIVITY_SERVICE);
        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnectedOrConnecting()) {
        } else {
            this.cancel(true);
            Toast.makeText(mContext, "Not connected to internet!",
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}

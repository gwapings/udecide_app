package org.app.udecide.helper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

public class AsyncBitmapLoader extends AsyncTask<String, Void, Bitmap> {

    private WeakReference<ImageView> imageViewReference;
    private String img_url;
    private Context mContext;
    private int mWidth = 0;
    private int mHeight = 0;

    public AsyncBitmapLoader(Context ctx, ImageView imgView, int width, int height) {
        imageViewReference = new WeakReference<ImageView>(imgView);
        mContext = ctx;
        mWidth = width;
        mHeight = height;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        img_url = params[0];
        Bitmap img = new UDecideHelper().getbmpfromURL(mContext, img_url);
        Bitmap bmp = (mWidth == 0 || mHeight == 0) ? img : Bitmap.createScaledBitmap(img, mWidth, mHeight, false);
        return bmp;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
            bitmap = null;
        }
        if (imageViewReference != null && bitmap != null) {
            final ImageView imageView = imageViewReference.get();
            final AsyncBitmapLoader bitmapWorkerTask =
                    getBitmapWorkerTask(imageView);
            if (this == bitmapWorkerTask && imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    public static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<AsyncBitmapLoader> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap,
                             AsyncBitmapLoader bitmapWorkerTask) {
            super(res, bitmap);
            bitmapWorkerTaskReference =
                    new WeakReference<AsyncBitmapLoader>(bitmapWorkerTask);
        }

        public AsyncBitmapLoader getBitmapWorkerTask() {
            return bitmapWorkerTaskReference.get();
        }
    }

    public static boolean cancelPotentialWork(String url, ImageView imageView) {
        final AsyncBitmapLoader bitmapWorkerTask = getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            final String bitmapData = bitmapWorkerTask.img_url;
            // If bitmapData is not yet set or it differs from the new data
            if (bitmapData == null || bitmapData != url) {
                // Cancel previous task
                bitmapWorkerTask.cancel(true);
            } else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }

    private static AsyncBitmapLoader getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }
        return null;
    }
}

package org.app.udecide.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.design.widget.NavigationView;
import android.view.MenuItem;
import android.view.SubMenu;

import org.app.udecide.Model.Categories;

public class LoadCustomMenuAsync extends AsyncTask<String, Void, Drawable> {

    public interface onCustomMenuClickListener {
        public void onLoadCustomMenuCompleted(MenuItem item, Categories category);
    }

    protected onCustomMenuClickListener mCustomMenuListener;
    protected Context mCustomMenuContext;
    protected Categories mCategory;
    protected NavigationView navView;

    public LoadCustomMenuAsync(Context ctx, Categories data, NavigationView nav_view, onCustomMenuClickListener listener) {
        mCustomMenuListener = listener;
        mCustomMenuContext = ctx;
        mCategory = data;
        navView = nav_view;
    }

    @Override
    protected Drawable doInBackground(String... params) {
        Bitmap icon = new UDecideHelper().getbmpfromURL(mCustomMenuContext, params[0]);
        Drawable d = new BitmapDrawable(mCustomMenuContext.getResources(), icon);
        return d;
    }

    @Override
    protected void onPostExecute(Drawable drawable) {
        MenuItem catGroup = navView.getMenu().getItem(0);
        SubMenu menu = catGroup.getSubMenu();

        MenuItem item = menu.add(mCategory.getName()).setIcon(drawable);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                mCustomMenuListener.onLoadCustomMenuCompleted(item, mCategory);
                return false;
            }
        });
    }
}

package org.app.udecide.helper;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.app.udecide.R;

public class UDecideConstants {
    public static final String BASE_URL = "http://udecide.upnext.shop/";
    public static final int NEARBY_DISTANCE = 5;
    public static final double CHECKOUT_DISTANCE = 0.05;

    public static Bitmap getDefaultImagePlaceholder(Context mContext) {
        return BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.blank_image);
    }
}

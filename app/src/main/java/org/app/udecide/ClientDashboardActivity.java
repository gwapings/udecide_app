package org.app.udecide;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.app.udecide.Model.Categories;
import org.app.udecide.Model.Restaurant;
import org.app.udecide.Parser.CategoriesParser;
import org.app.udecide.Parser.RestaurantParser;
import org.app.udecide.helper.AsyncBitmapLoader;
import org.app.udecide.helper.LoadCustomMenuAsync;
import org.app.udecide.helper.UDecideBackgroundService;
import org.app.udecide.helper.UDecideConstants;
import org.app.udecide.helper.UDecideHelper;
import org.app.udecide.helper.UDecideLocationListener;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

import vibetech.helper.httpclienthelper.HEADERS;
import vibetech.helper.httpclienthelper.HttpClientHelper;
import vibetech.helper.httpclienthelper.HttpClientInterface;
import vibetech.helper.httpclienthelper.ToastHandler;

public class ClientDashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, UDecideLocationListener.UDecideLocationListenerInterface, OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        FragmentRestaurants.OnFragmentInteractionListener,
        FragmentRestaurantMenu.OnFragmentInteractionListener,
        FragmentAdvanceOrder.OnFragmentInteractionListener,
        FragmentAdvanceOrderView.OnFragmentInteractionListener,
        FragmentReservations.OnFragmentInteractionListener,
        HttpClientInterface, LoadCustomMenuAsync.onCustomMenuClickListener {

    protected static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    protected static final int CONTENT_VIEW_ID = 10101010;
    protected Toolbar toolbar;
    protected DrawerLayout drawer;
    protected ActionBarDrawerToggle toggle;
    protected NavigationView navigationView;
    protected HashMap<Marker, Restaurant> markerMap = new HashMap<>();

    protected Location mLastLocation;

    protected GoogleMap mGoogleMap;
    protected SupportMapFragment fragMap;
    protected Marker mCurrLocationMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_dashboard);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);
        setCategoriesMenu();

        fragMap = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        fragMap.getMapAsync(this);
        new UDecideLocationListener(this, this);

        View vw = getLayoutInflater().inflate(R.layout.nav_header_client_dashboard, navigationView);
        TextView txUsername = (TextView) vw.findViewById(R.id.nav_user_name);
        txUsername.setText(new UDecideHelper().getUsername(ClientDashboardActivity.this));

        ImageView user_avatar = (ImageView) vw.findViewById(R.id.nav_user_avatar);
        if (AsyncBitmapLoader.cancelPotentialWork(UDecideConstants.BASE_URL + new UDecideHelper().getUserAvatar(getApplicationContext()), user_avatar)) {
            AsyncBitmapLoader task = new AsyncBitmapLoader(getApplicationContext(), user_avatar, 120, 120);
            AsyncBitmapLoader.AsyncDrawable asyncDrawable =
                    new AsyncBitmapLoader.AsyncDrawable(getResources(), UDecideConstants.getDefaultImagePlaceholder(getApplicationContext()), task);
            user_avatar.setImageDrawable(asyncDrawable);
            task.execute(UDecideConstants.BASE_URL + new UDecideHelper().getUserAvatar(getApplicationContext()));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Start range validation service
        Intent service = new Intent(this, UDecideBackgroundService.class);
        startService(service);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Stop range validation service
        Intent service = new Intent(this, UDecideBackgroundService.class);
        stopService(service);
    }

    protected void setCategoriesMenu() {
        String categoriesUrl = UDecideConstants.BASE_URL + "api/categories/list";
        new HttpClientHelper().executeInBackground(this, categoriesUrl, null, null, false, false, new ArrayList<HEADERS>(), new HttpClientInterface() {

            ArrayList<Categories> categories = new ArrayList<Categories>();

            @Override
            public void onBeforeExecute(int i) {

            }

            @Override
            public void onHttpClientResponse(int i, String s) {
                try {
                    categories = new CategoriesParser().parseCategories(s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onHttpClientErrorException(int i, Exception e) {

            }

            @Override
            public void onAfterExecute(int i) {
                if (categories.size() > 0) {
                    for (final Categories category : categories) {
                        LoadCustomMenuAsync menu_loader = new LoadCustomMenuAsync(getApplicationContext(), category, navigationView, ClientDashboardActivity.this);
                        menu_loader.execute(UDecideConstants.BASE_URL + category.getIcon_path());
                    }
                }
            }
        }, 24120);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.client_dashboard, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_logout) {
            new UDecideHelper().logout(this);
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if ((id == R.id.nav_restaurants) || (id == R.id.nav_nearby)) {
            Fragment mFragment = new FragmentRestaurants();
            Bundle bundle = new Bundle();
            if (id == R.id.nav_nearby) {
                bundle.putBoolean("is_nearby", true);
                bundle.putParcelable("mLastLocation", mLastLocation);
            } else {
                bundle.putBoolean("is_nearby", false);
            }

            mFragment.setArguments(bundle);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frag_container, mFragment, "restaurant")
                    .addToBackStack("restaurants")
                    .commit();
        } else if (id == R.id.nav_advance_order) {
            Fragment mFragment = new FragmentAdvanceOrderView();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frag_container, mFragment, "advance_order_view")
                    .addToBackStack("restaurants")
                    .commit();
        } else if (id == R.id.nav_reservation) {
            Fragment mFragment = new FragmentReservations();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frag_container, mFragment, "reservations")
                    .addToBackStack("restaurants")
                    .commit();
        }

        getSupportFragmentManager().addOnBackStackChangedListener(backstack_listener);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private FragmentManager.OnBackStackChangedListener backstack_listener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true); // show back button
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
                fragMap.getView().setVisibility(View.INVISIBLE);
            } else {
                //show hamburger
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                toggle.syncState();
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        drawer.openDrawer(GravityCompat.START);
                    }
                });
                fragMap.getView().setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public void onLoadCustomMenuCompleted(MenuItem item, Categories category) {
        Fragment mFragment = new FragmentRestaurants();

        Bundle bundle = new Bundle();
        bundle.putBoolean("is_nearby", true);
        bundle.putParcelable("category", category);

        mFragment.setArguments(bundle);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frag_container, mFragment, "restaurant")
                .addToBackStack("restaurants")
                .commit();
    }

    private void getRestaurants() {
        HEADERS headers = new HEADERS();
        headers.addHeaders("UDecide_USERID", new UDecideHelper().getUserId(getApplicationContext()));
        String restaurantListURL = UDecideConstants.BASE_URL + "api/restaurant/list";
        new HttpClientHelper().executeInBackground(this, restaurantListURL, null, null, false, false, headers.getArrHeaders(), this, 1001);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.setOnMarkerClickListener(this);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                checkLocationPermission();
            }
        } else {
            mGoogleMap.setMyLocationEnabled(true);
        }
        getRestaurants();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        //Place current location marker
        LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        renderMyLocation(latLng);
        getRestaurants();
    }

    @Override
    public void onConnected() {

    }

    @Override
    public void onDisconnected() {

    }

    public void renderMyLocation(LatLng latlon) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latlon);
        markerOptions.title("You");
//        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.app_icon));
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latlon));
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        mGoogleMap.setMyLocationEnabled(true);
                    }

                } else {

                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    @Override
    public void onBeforeExecute(int i) {
    }

    @Override
    public void onHttpClientResponse(int i, String s) {
        ArrayList<Restaurant> arrRestaurants = new RestaurantParser().parseRestaurant(s);
        renderRestaurantsOnMap(arrRestaurants);
    }

    @Override
    public void onHttpClientErrorException(int i, Exception e) {
        Log.d("TEST", "[onHttpClientErrorException][Exception]" + e.toString());
    }

    @Override
    public void onAfterExecute(int i) {
    }

    private void renderRestaurantsOnMap(ArrayList<Restaurant> arr_restaurants) {
        markerMap = new HashMap<>();
        mGoogleMap.clear();
        UDecideHelper helper = new UDecideHelper();
        for (Restaurant restaurant : arr_restaurants) {
            if (mLastLocation != null) {
                double distance = helper.getDistanceBetweenLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), restaurant.getLocation().getLatitude(), restaurant.getLocation().getLongitude());
                if (distance <= UDecideConstants.NEARBY_DISTANCE) {
                    LatLng latlon = new LatLng(restaurant.getLocation().getLatitude(), restaurant.getLocation().getLongitude());

                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latlon);
                    markerOptions.title(restaurant.getResto_name());
                    markerOptions.snippet(restaurant.getCategory().getType());
                    if (restaurant.getThumbnail().getFile_path() == null) {
                        Bitmap mIcon = BitmapFactory.decodeResource(getResources(),
                                R.mipmap.app_icon);
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(mIcon, 120, 120, false);
                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap));
                    } else {
                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(new UDecideHelper().getbmpfromURL(this, UDecideConstants.BASE_URL + restaurant.getThumbnail().getFile_path()), 100, 100, false);
                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap));
                    }
                    Marker marker = mGoogleMap.addMarker(markerOptions);
                    marker.showInfoWindow();
                    markerMap.put(marker, restaurant);
                }
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Restaurant restaurant = (Restaurant) markerMap.get(marker);
        if (restaurant != null) {
//            ToastHandler.ShowToast(this, restaurant.getResto_name());
            if (!new UDecideHelper().isGuest(this)) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("restaurant", restaurant);
                bundle.putBoolean("is_request_seat", false);

                Fragment mFragment = new FragmentRestaurantMenu();
                mFragment.setArguments(bundle);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frag_container, mFragment, "restaurant")
                        .addToBackStack("restaurant_menu")
                        .commit();
                getSupportFragmentManager().addOnBackStackChangedListener(backstack_listener);
            } else {
                showAlertRegistration();
            }
        }
        return false;
    }

    private void showAlertRegistration() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle("Sign-Up Now?");
        mBuilder.setMessage("You need to signup before you can access the full content.");
        mBuilder.setPositiveButton("Sign Up", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new UDecideHelper().logout(ClientDashboardActivity.this);

                startActivity(new Intent(ClientDashboardActivity.this, RegistrationActivity.class));
                ClientDashboardActivity.this.finish();
            }
        }).setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        mBuilder.create().show();
    }
}
